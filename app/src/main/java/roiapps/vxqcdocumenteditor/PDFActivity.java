package roiapps.vxqcdocumenteditor;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.json.JSONArray;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by Roi on 1/6/2017.
 */

public class PDFActivity extends Activity {

    public static JSONArray mDocument;

    private LinearLayout mEditorLayout;
    private FunnelPDF mFunnelPDF;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pdf_activity);

        mEditorLayout = (LinearLayout) findViewById(R.id.pdf_editor);

        if(mDocument != null){
            JSONDocumentParser jsonDocumentParser = new JSONDocumentParser(this);
            DocumentStructure documentStructure = jsonDocumentParser.GetDocumentStructure(mDocument);
            jsonDocumentParser.PutLineToLayout(documentStructure, mEditorLayout);

            mFunnelPDF = new FunnelPDF(this, new Document(), documentStructure, mEditorLayout);
        }

        mEditorLayout.post(new Runnable() {
            @Override
            public void run() {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        mFunnelPDF.Install();
                    }
                });

                new CreatePDF().execute();
            }
        });
    }

    class CreatePDF extends AsyncTask<Void, Void, Void> {

        ProgressDialog mProgressDialog = new ProgressDialog(PDFActivity.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage("Load PDF");
            mProgressDialog.show();

        }

        @Override
        protected Void doInBackground(Void... voids) {
            mEditorLayout.post(new Runnable() {
                @Override
                public void run() {
                    mFunnelPDF.GetPDF(new FunnelPDF.OnRetuen() {
                        @Override
                        public void OnReturn(final FunnelPDF.PrintedPdfDocument PrintedPdfDocument) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),"VX_PDF.pdf");

                                    try {
                                        PrintedPdfDocument.writeTo(new FileOutputStream(file));
                                        PrintedPdfDocument.close();

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        throw new RuntimeException("Fail To Create PDF");
                                    }

                                    try {
                                        Intent intent = new Intent(Intent.ACTION_VIEW);
                                        intent.setDataAndType(Uri.fromFile(file), "application/pdf");
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                        startActivity(intent);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                }
                            }).start();
                        }
                    });
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            mProgressDialog.cancel();

        }
    }

}
