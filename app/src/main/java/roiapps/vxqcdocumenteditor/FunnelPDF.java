package roiapps.vxqcdocumenteditor;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.pdf.PdfDocument;
import android.print.PrintAttributes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TextView;

import java.lang.*;
import java.util.ArrayList;

public class FunnelPDF {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private Document mDocument;

    private int mPagesCounter = 0;
    private String mPagesNumber = "";

    private PrintedPdfDocument mPrintedPdfDocument;

    private PdfDocument.Page mThisPage;
    private Canvas mThisPageCanvas;
    private int mThisPageHeight;
    private int mThisPageYCounter = 0;

    private View mLowerTableView;
    private int mLowerTableHeight = 0;

    private static final int mPushFromContent = 100;

    private DocumentStructure mDocumentStructure;

    private ViewGroup mViewGroup;

    private boolean mThereIsFailuresTable = false;

    private static final int mAddToTextSize = 5;

    public FunnelPDF(Context Context, Document Document, DocumentStructure DocumentStructure, ViewGroup ViewGroup){
        mContext = Context;
        mDocument = Document;
        mDocumentStructure = DocumentStructure;
        mViewGroup = ViewGroup;

        mLayoutInflater = (LayoutInflater) mContext.getSystemService(android.content.Context.LAYOUT_INFLATER_SERVICE);

        for(int i = 0; i < mDocumentStructure.mObjectsArrayList.size(); i++){
            Object object = mDocumentStructure.mObjectsArrayList.get(i);
            View objectView = (View) object.mObjectView;

            if(objectView instanceof CheckBox){
                CheckBox spasificObject = (CheckBox) objectView;
                spasificObject.setTextSize(spasificObject.getTextSize() + mAddToTextSize);

            } else if(objectView instanceof TextView){
                TextView spasificObject = (TextView) objectView;
                spasificObject.setTextSize(spasificObject.getTextSize() + mAddToTextSize);

            } else if(objectView instanceof EditText) {
                EditText spasificObject = (EditText) objectView;
                spasificObject.setTextSize(spasificObject.getTextSize() + mAddToTextSize);

            }
        }

        return;
    }

    public void Install(){
        CreateDocument(mViewGroup);

        AddUpperTable(mViewGroup, mDocumentStructure);
        mThereIsFailuresTable = AddFailuresTable(mViewGroup, mDocumentStructure);

        return;
    }

    public void GetPDF(OnRetuen OnRetuen){
        mThisPageHeight = mPrintedPdfDocument.getPageHeightWithoutMargins() - mLowerTableHeight;

        mPagesNumber = Integer.toString(GetPageCount(mDocumentStructure, mThereIsFailuresTable) + 1);

        CreateNextPage();

        int size = mDocumentStructure.mLineArrayList.size();
        for (int i = 0; i < size; i++) {
            Line thisLine = mDocumentStructure.mLineArrayList.get(i);

            if(mThereIsFailuresTable && i == size - 1){
                mThisPageYCounter = mThisPageHeight + 1;
            }

            if(thisLine.mLineKind == Line.LINEKIND_LINEAR && thisLine.mOrientation == Line.OBJECTORIENTATION_VERTICAL){
                for (int j = 0; j < thisLine.mInLineObjects.size(); j++) {
                    Object thisObject = thisLine.mInLineObjects.get(j);
                    AddViewToPDF(thisObject.mObjectView, thisObject.mMarginLeft + thisLine.mPaddingLeft);

                    ((ViewGroup) thisObject.mObjectView.getParent()).removeView(thisObject.mObjectView);
                }

            } else {
                AddViewToPDF(thisLine.mViewGroup, 0);

                ((ViewGroup) (thisLine.mViewGroup).getParent()).removeView(thisLine.mViewGroup);
            }
        }

        CloseThisPage();

        OnRetuen.OnReturn(mPrintedPdfDocument);

        return;
    }

    private void AddViewToPDF(View View, int MarginsLeft){
        int thisObjectHeight = View.getHeight();
        int thisObjectWidth = View.getWidth();

        boolean viewInNewPage = false;
        if(isObjectInNewLine(thisObjectHeight)){
            CloseThisPage();
            CreateNextPage();

            viewInNewPage = true;
        }

        int topMargins = 0;
        int bottomMargins = 0;

        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) View.getLayoutParams();
        int marginTop = layoutParams.topMargin;
        int marginBottom = layoutParams.bottomMargin;

         if(!viewInNewPage){
             if(!isObjectInNewLine(thisObjectHeight + marginTop + marginBottom)){
                 topMargins = marginTop;
                 bottomMargins = marginBottom;

             } else if(!isObjectInNewLine(thisObjectHeight + marginTop)){
                 topMargins = marginTop;
             }

         } else {
             bottomMargins = marginBottom;
         }

        if(thisObjectHeight == 0){
            return;
        }

        Bitmap thisObjectBitmap = Bitmap.createBitmap(thisObjectWidth, thisObjectHeight, Bitmap.Config.ARGB_4444);
        Canvas thisObjectCanvas = new Canvas(thisObjectBitmap);

        View.draw(thisObjectCanvas);

        mThisPageCanvas.drawBitmap(thisObjectBitmap, MarginsLeft, mThisPageYCounter + topMargins, null);

        thisObjectBitmap.recycle();

        mThisPageYCounter += thisObjectHeight + bottomMargins;

        return;
    }

    private boolean isObjectInNewLine(int Height){
        return (mThisPageYCounter + Height) > mThisPageHeight;
    }

    //Page Control

    private void CreateNextPage(){
        mThisPage = mPrintedPdfDocument.startPage(mPagesCounter);
        mThisPageCanvas = mThisPage.getCanvas();

        mThisPageYCounter = 0;

        return;
    }

    private void CloseThisPage(){
        AddLowerTable(mPagesCounter);

        mPrintedPdfDocument.finishPage(mThisPage);

        mThisPage = null;
        mThisPageCanvas = null;
        mThisPageYCounter = 0;

        mPagesCounter++;

        return;
    }

    private int GetPageCount(DocumentStructure DocumentStructure, boolean thereIsFailuresTable){
        int counter = 0;

        int size = DocumentStructure.mLineArrayList.size();
        for (int i = 0; i < size; i++) {
            Line thisLine = DocumentStructure.mLineArrayList.get(i);

            if(thereIsFailuresTable && i == size - 1){
                mThisPageYCounter = mThisPageHeight + 1;
            }

            if(thisLine.mLineKind == Line.LINEKIND_LINEAR && thisLine.mOrientation == Line.OBJECTORIENTATION_VERTICAL){
                for (int j = 0; j < thisLine.mInLineObjects.size(); j++) {
                    Object thisObject = thisLine.mInLineObjects.get(j);

                    counter += GetOneIfNewPage(thisObject.mObjectView);
                }

            } else {
                counter += GetOneIfNewPage(thisLine.mViewGroup);

            }
        }

        return counter;
    }

    private int GetOneIfNewPage(View View){
        int returnValue = 0;
        int thisObjectHeight = View.getHeight();

        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) View.getLayoutParams();
        int marginTop = layoutParams.topMargin;
        int marginBottom = layoutParams.bottomMargin;

        if((mThisPageYCounter + thisObjectHeight) > mThisPageHeight){
            returnValue = 1;
            mThisPageYCounter = 0;
        }

        mThisPageYCounter += thisObjectHeight;

        if(returnValue == 0){
            if(!isObjectInNewLine(thisObjectHeight + marginTop + marginBottom)){
                mThisPageYCounter += marginTop;
                mThisPageYCounter += marginBottom;

            } else if(!isObjectInNewLine(thisObjectHeight + marginTop)){
                mThisPageYCounter += marginTop;
            }

        } else {
            mThisPageYCounter += marginBottom;
        }

        return returnValue;
    }


    /*************************************************************************************************/


    //Create Document

    private void CreateDocument(ViewGroup ViewGroup){
        mPrintedPdfDocument = new PrintedPdfDocument(GetPrintAttributes());

        SetA4PageSizes(ViewGroup, mPrintedPdfDocument);

        mLowerTableView = GetNextLowerTable();
        ViewGroup.addView(mLowerTableView);

        mLowerTableView.measure(0, 0);

        mLowerTableHeight = mLowerTableView.getMeasuredHeight() + mPushFromContent;

        return;
    }

    private PrintAttributes GetPrintAttributes(){
        PrintAttributes.Builder printAttributes = new PrintAttributes.Builder();

        printAttributes.setColorMode(PrintAttributes.COLOR_MODE_MONOCHROME);
        printAttributes.setMediaSize(PrintAttributes.MediaSize.ISO_A4);
        printAttributes.setMinMargins(new PrintAttributes.Margins(600, 650, 600, 300));

        return printAttributes.build();
    }

    private void SetA4PageSizes(ViewGroup ViewGroup, PrintedPdfDocument PrintedPdfDocument){
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(PrintedPdfDocument.getPageWidthWithoutMargins(), FrameLayout.LayoutParams.WRAP_CONTENT);
        ViewGroup.setLayoutParams(params);
        ViewGroup.setPadding(0, 0, 0, 0);

        return;
    }

    /*************************************************************************************************/


    //Lower Table

    private void AddLowerTable(int ThisPageNumber){
        GetNextLowerTable(mLowerTableView, Integer.toString(ThisPageNumber + 1) + " Of " + mPagesNumber);

        Bitmap thisLowerTableBitmap = Bitmap.createBitmap(mLowerTableView.getMeasuredWidth(), mLowerTableView.getHeight(), Bitmap.Config.RGB_565);
        Canvas thisLowerTableCanvas = new Canvas(thisLowerTableBitmap);

        mLowerTableView.draw(thisLowerTableCanvas);

        mThisPageCanvas.drawBitmap(thisLowerTableBitmap, 0, mThisPageHeight + mPushFromContent, null);

        thisLowerTableBitmap.recycle();

        return;
    }

    private View GetNextLowerTable(){
        View lowerTableLayout = mLayoutInflater.inflate(R.layout.lowertable, null);

        return GetNextLowerTable(lowerTableLayout, "");
    }

    private View GetNextLowerTable(View lowerTableLayout, String Page){
        TextView fileNameTextView = (TextView) lowerTableLayout.findViewById(R.id.FileNameTextView);
        TextView revisionTextView = (TextView) lowerTableLayout.findViewById(R.id.RevisionTextView);
        TextView pageTextView = (TextView) lowerTableLayout.findViewById(R.id.PageTextView);
        TextView dateTextView = (TextView) lowerTableLayout.findViewById(R.id.DateTextView);

        pageTextView.setText(Page);
        fileNameTextView.setText(mDocument.mDocumentSource.mPartNumber + " rev " + mDocument.mDocumentSource.mRevision + ".doc");
        revisionTextView.setText(mDocument.mDocumentSource.mRevision);
        dateTextView.setText(mDocument.mDocumentSource.mDate);

        return lowerTableLayout;
    }


    //Upper Table

    private ViewGroup CreateUpperrTable(){
        ViewGroup layout = (ViewGroup) mLayoutInflater.inflate(R.layout.uppertable, null);

        TextView documentTypeTextView = (TextView) layout.findViewById(R.id.DocumentTypeTextView);
        TextView documentPNTextView = (TextView) layout.findViewById(R.id.DocumentPNTextView);
        TextView documentNameTextView = (TextView) layout.findViewById(R.id.DocumentNameTextView);
        TextView partNumberTextView = (TextView) layout.findViewById(R.id.PartNumberTextView);
        TextView descriptionTextView = (TextView) layout.findViewById(R.id.DescriptionTextView);
        TextView purposeTextView = (TextView) layout.findViewById(R.id.PurposeTextView);
        TextView mainProductTextView = (TextView) layout.findViewById(R.id.MainProductTextView);

        TextView testerNameTextView = (TextView) layout.findViewById(R.id.TesterNameTextView);
        TextView dateTextView = (TextView) layout.findViewById(R.id.DateTextView);
        ImageView signatrueImageView = (ImageView) layout.findViewById(R.id.SignatrueImageView);

        TableLayout identificationTableLayout = (TableLayout) layout.findViewById(R.id.IdentificationTableLayout);

        documentTypeTextView.setText(mDocument.mDocumentSource.mType);
        documentPNTextView.setText(mDocument.mDocumentSource.mPN);
        documentNameTextView.setText(mDocument.mDocumentSource.mDocumentName);
        partNumberTextView.setText(mDocument.mDocumentSource.mPartNumber);
        descriptionTextView.setText(mDocument.mDocumentSource.mDescription);
        purposeTextView.setText(mDocument.mDocumentSource.mPurpose);
        mainProductTextView.setText(mDocument.mDocumentSource.mProduct);

        testerNameTextView.setText(mDocument.mTester);
        dateTextView.setText("Simple Date");
        signatrueImageView.setImageBitmap(mDocument.mSignatrue);

        for (Identification identification : mDocument.mDocumentSource.mIdentificationList) {
            View identificationLayout = mLayoutInflater.inflate(R.layout.identificationitem, null);

            TextView nameTextView = (TextView) identificationLayout.findViewById(R.id.Name);
            TextView valueTextView = (TextView) identificationLayout.findViewById(R.id.Value);

            nameTextView.setText(identification.mName);
            valueTextView.setText(identification.mValue);

            identificationTableLayout.addView(identificationLayout);
        }

        return layout;
    }

    private void AddUpperTable(ViewGroup ViewGroup, DocumentStructure DocumentStructure){
        ViewGroup upperTable = CreateUpperrTable();
        ViewGroup.addView(upperTable, 0);

        Line line = new Line();
        line.mViewGroup = upperTable;
        line.mLineKind = -1;

        DocumentStructure.mLineArrayList.add(0, line);

        return;
    }


    //Failures Table

    private ViewGroup CreateFailuresTable(){
        ArrayList<Failure> failuresList = mDocument.mFailuresList;

        if(failuresList == null){
            return null;
        }

        int size;
        if((size = failuresList.size()) == 0){
            return null;
        }

        LinearLayout linearLayout = new LinearLayout(mContext);
        linearLayout.setOrientation(LinearLayout.VERTICAL);

        linearLayout.addView(mLayoutInflater.inflate(R.layout.failurestitletable, null));

        for (int i = 0; i < size; i++) {
            Failure failure = failuresList.get(i);
            View layout = mLayoutInflater.inflate(R.layout.failuretableitem, null);

            TextView mainTextView = (TextView) layout.findViewById(R.id.MainTextView);
            TextView subTextView = (TextView) layout.findViewById(R.id.SubTextView);
            TextView explTextView = (TextView) layout.findViewById(R.id.ExplTextView);
            TextView failureDescriptionTextView = (TextView) layout.findViewById(R.id.FailureDescriptionTextView);
            TextView actionTakenTextView = (TextView) layout.findViewById(R.id.ActionTakenTextView);
            TextView technicainTextView = (TextView) layout.findViewById(R.id.TechnicainTextView);

            mainTextView.setText(failure.mFailureSource.mMain);
            subTextView.setText(failure.mFailureSource.mSub);
            explTextView.setText(failure.mFailureSource.mExpl);

            failureDescriptionTextView.setText(failure.mDescription);
            actionTakenTextView.setText(failure.mCorrectiveAction);

            technicainTextView.setText(failure.mTechnician);

            linearLayout.addView(layout);
        }

        return linearLayout;
    }

    private boolean AddFailuresTable(ViewGroup ViewGroup, DocumentStructure DocumentStructure){
        android.view.ViewGroup failuresTable = CreateFailuresTable();

        if(failuresTable != null) {
            Line line = new Line();

            line.mViewGroup = failuresTable;
            line.mLineKind = -1;

            ViewGroup.addView(line.mViewGroup);
            DocumentStructure.mLineArrayList.add(line);

            return true;
        }

        return false;
    }

    /*************************************************************************************************/


    //Class

    public interface OnRetuen {
        void OnReturn(PrintedPdfDocument PrintedPdfDocument);
    }

    public class PrintedPdfDocument extends PdfDocument {
        private final int mPageWidth;
        private final int mPageHeight;

        private final Rect mContentRect;

        public int mPDFTopMargin;
        public int mPDFLeftMargin;
        public int mPDFRightMargin;
        public int mPDFBottomMargin;

        public PrintedPdfDocument(PrintAttributes attributes) {
            mPageWidth = (int) Dimensions.mmToPx(210.0f);
            mPageHeight = (int) Dimensions.mmToPx(297.0f);

            PrintAttributes.Margins minMargins = attributes.getMinMargins();
            final int marginTop = (int) Dimensions.milsToPx(minMargins.getTopMils());
            final int marginLeft = (int) Dimensions.milsToPx(minMargins.getLeftMils());
            final int marginRight = (int) Dimensions.milsToPx(minMargins.getRightMils());
            final int marginBottom = (int) Dimensions.milsToPx(minMargins.getBottomMils());

            mPDFTopMargin = marginTop;//(int) Dimensions.milsToPx(minMargins.getTopMils());
            mPDFLeftMargin = marginLeft;//(int) Dimensions.milsToPx(minMargins.getLeftMils());
            mPDFRightMargin = marginRight;//(int) Dimensions.milsToPx(minMargins.getRightMils());
            mPDFBottomMargin = marginBottom;//(int) Dimensions.milsToPx(minMargins.getBottomMils());

            mContentRect = new Rect(marginLeft, marginTop, mPageWidth - marginRight, mPageHeight - marginBottom);
        }

        public Page startPage(int pageNumber) {
            PageInfo pageInfo = new PageInfo
                    .Builder(mPageWidth, mPageHeight, pageNumber)
                    .setContentRect(mContentRect)
                    .create();
            return startPage(pageInfo);
        }

        public int getPageWidthWithoutMargins(){
            return mPageWidth - mPDFLeftMargin - mPDFRightMargin;
        }

        public int getPageHeightWithoutMargins(){
            return mPageHeight - mPDFTopMargin - mPDFBottomMargin;
        }

    }

}