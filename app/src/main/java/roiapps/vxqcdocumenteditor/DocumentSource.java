package roiapps.vxqcdocumenteditor;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;

public class DocumentSource extends DynamicObject {

    public String mProduct;
    public String mType;
    public String mRevision;

    @Nullable
    public Bitmap mImage;

    @NonNull
    public ArrayList<Identification> mIdentificationList = new ArrayList<Identification>();

    public String mPN;
    public String mDocumentName;
    public String mPartNumber;
    public String mDescription;
    public String mPurpose;
    public String mDate;
}
