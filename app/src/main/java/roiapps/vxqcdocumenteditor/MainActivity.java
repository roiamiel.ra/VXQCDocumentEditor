package roiapps.vxqcdocumenteditor;

import android.animation.Animator;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.AsyncQueryHandler;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ShapeDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;

public class MainActivity extends Activity {

    DocumentStructure mDocumentStructure;

    //DocumentLayout
    /******************************************************/
    RelativeLayout mDocumentRelativeLayout;
    /******************************************************/

    //DocumentCreator
    /******************************************************/
    DocumentCreator mDocumentCreator;
    /******************************************************/

    //Selected Line
    /******************************************************/
    Line mSelectedLine;
    /******************************************************/

    //Manus
    /******************************************************/
    RelativeLayoutAnimate mMainMenu;
    RelativeLayoutAnimate mLineSettingsMenu;
    RelativeLayoutAnimate mObjectSettingsMenu;
    /******************************************************/

    //Scroll View
    /******************************************************/
    ScrollView mLayoutScrollView;
    /******************************************************/

    //Main Menu
    /******************************************************/

    //AddNew
    Button mMainMenuAddNewLineButton;
    Button mMainMenuAddNewTextViewButton;
    Button mMainMenuAddNewEditTextButton;
    Button mMainMenuAddNewCheckBoxButton;

    //Tools
    Button mMainMenuToolsEraserButton;
    Button mMainMenuToolsCopyLineButton;

    //Options
    Button mMainMenuOptionsSaveButton;
    Button mRebuildObjectsButton;
    Button mShowPDFButton;

    /******************************************************/

    //Line Settings Menu
    /******************************************************/

    //Kind
    RadioGroup mLineSettingsKindLineRadioGroup;
    RadioButton mLineSettingsKindLinearRadioButton;
    RadioButton mLineSettingsKindTableRadioButton;
    RadioButton mLineSettingsKindXYRadioButton;

    //Position
    EditText mLineSettingsPositionEditText;

    //Padding
    EditText mLineSettingsPaddingLeftEditText;

    //Oriention
    RadioGroup mLineSettingsOrientionRadioGroup;
    RadioButton mLineSettingsOrientionHorizontalRadioButton;
    RadioButton mLineSettingsOrientionVerticalRadioButton;

    //RowsAndColums
    RelativeLayout mRowsAndColumsLayout;
    EditText mRowsAndColumsLayoutRowsEditText;
    EditText mRowsAndColumsLayoutColumsEditText;

    EditText mNoViableListEditText;

    //Done
    Button mLineSettingsDoneButton;

    /******************************************************/

    //Object Settings Menu
    /******************************************************/

    //Text
    RelativeLayout mObjectSettingsTextMenuRelativeLayout;
    EditText mObjectSettingsTextTextEditText;
    Spinner mObjectSettingsTextTextColorEditText;

    AutoCompleteCached mTextSizeAutoCompleteCached = new AutoCompleteCached<Integer>(App.mContext, new NameAndValue[]{
                                                                                                                new NameAndValue("Title", 26),
                                                                                                                new NameAndValue("Sub Title", 23),
                                                                                                                new NameAndValue("Big Text", 22),
                                                                                                                new NameAndValue("Text", 21),
                                                                                                                new NameAndValue("Small Text", 19)});
    AutoCompleteTextView mObjectSettingsTextSizeAutoCompleteTextView;

    CheckBox mObjectSettingsTextBoldCheckBox;
    CheckBox mObjectSettingsTextItalicCheckBox;

    //Typing
    RelativeLayout mObjectSettingsTypingMenuRelativeLayout;
    EditText mObjectSettingsTypingHintEditText;
    EditText mObjectSettingsTypingUpperLimitEditText;
    EditText mObjectSettingsTypingLowerLimitEditText;

    CheckBox mObjectSettingsTypingNumbersKeyboardCheckBox;

    //Text Value
    RelativeLayout mObjectSettingsTextValueMenuRelativeLayout;
    EditText mObjectSettingsTextValueValueEditText;

    //Sizes
    RelativeLayout mObjectSettingsSizesMenuRelativeLayout;
    EditText mObjectSettingsSizesXSizeEditText;
    EditText mObjectSettingsSizesYSizeEditText;

    //Coordinates
    RelativeLayout mObjectSettingsCoordinatesMenuRelativeLayout;
    EditText mObjectSettingsCoordinatesXEditText;
    EditText mObjectSettingsCoordinatesYEditText;

    //Margins
    RelativeLayout mObjectSettingsMarginsMenuRelativeLayout;
    EditText mObjectSettingsMarginsTopEditText;
    EditText mObjectSettingsMarginsBottomEditText;
    EditText mObjectSettingsMarginsLeftEditText;
    EditText mObjectSettingsMarginsRightEditText;

    //Boolean Value
    RelativeLayout mObjectSettingsBooleanValueMenuRelativeLayout;
    CheckBox  mObjectSettingsBooleanValueValueCheckBox;

    //Done Button
    Button mObjectSettingsDoneButton;
    Button mObjectSettingsDeleteButton;

    RelativeLayout mObjectNameLayout;
    EditText mObjectSettingsNameEditText;

    boolean mEditLineState = false;

    /******************************************************/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        JSONArray thisFile = null;

        File file = new File(Environment.getExternalStorageDirectory() + "/VXDocument.txt");
        if(file.exists()){
            try {
                BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF8"));

                StringBuilder text = new StringBuilder();

                String line = "";
                while ((line = in.readLine()) != null) {
                    text.append(line);
                }

                thisFile = new JSONArray(text.toString());

                in.close();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        App.mDisplayMetrics = getResources().getDisplayMetrics();

        mDocumentCreator = new DocumentCreator(MainActivity.this);

        mDocumentRelativeLayout = (RelativeLayout) findViewById(R.id.EditorRelativeLayout);
        mDocumentRelativeLayout.addView(mDocumentCreator.getBaseLayout());

        if(thisFile != null){
            mDocumentStructure = mDocumentCreator.mJSONDocumentParser.GetDocumentStructure(thisFile);
            mDocumentCreator.mJSONDocumentParser.PutLineToLayout(mDocumentStructure, mDocumentCreator.getBaseLayout(),
                    new JSONDocumentParser.OnAddLine() {
                        @Override
                        public void OnAddLine(LinearLayout AddTo, Line Line) {
                            //mDocumentCreator.AddLine(Line);

                            final Line finalLine = Line;
                            Line.mViewGroup.setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    UpdateSelectedLine(finalLine);
                                }
                            });

                            Line.mViewGroup.setOnLongClickListener(new View.OnLongClickListener() {
                                @Override
                                public boolean onLongClick(View v) {
                                    UpdateLine(finalLine);

                                    return false;
                                }
                            });

                            AddTo.addView(Line.mViewGroup);
                            mDocumentCreator.mLinesArray.add(Line);
                        }
                    }, new JSONDocumentParser.OnAddObject() {
                        @Override
                        public void OnAddObject(ViewGroup AddTo, Object Object, Line Line) {
                            AddTo.addView(Object.mObjectView);
                            new OnLongClickOpenToEditListener(Object, Line.mLinePosition, Line);

                        }
                    });

        }

        int size = mDocumentCreator.mLinesArray.size();
        if(size > 0) {
            UpdateSelectedLine(mDocumentCreator.mLinesArray.get(size - 1));
        }

        installMenus();

        setMainMenuVisible();
    }


    public void UpdateLine(Line Line){
        setLineSettingsMenuVisible(Line);
    }

    public void installMenus(){
        mMainMenu = (RelativeLayoutAnimate) findViewById(R.id.MainMenu);
        mLineSettingsMenu = (RelativeLayoutAnimate) findViewById(R.id.LineSettingsMenu);
        mObjectSettingsMenu = (RelativeLayoutAnimate) findViewById(R.id.ObjectSettingsMenu);

        mLayoutScrollView = (ScrollView) findViewById(R.id.ScrollView);

        return;
    }

    public void restartScreen(){
        mMainMenu.setVisibility(View.GONE);
        mLineSettingsMenu.setVisibility(View.GONE);
        mObjectSettingsMenu.setVisibility(View.GONE);

        return;
    }

    public void setLineSettingsMenuVisible(Line Line){
        mEditLineState = true;

        restartScreen();
        mLineSettingsMenu.setVisibility(View.VISIBLE);
        loadLineSettingsMenu();

        mLineSettingsPositionEditText.setText(Integer.toString(Line.mLinePosition));
        mLineSettingsPaddingLeftEditText.setText(Integer.toString(Line.mPaddingLeft));

        mLineSettingsKindLineRadioGroup.setEnabled(false);
        mLineSettingsKindTableRadioButton.setEnabled(false);
        mLineSettingsKindLinearRadioButton.setEnabled(false);
        mLineSettingsKindXYRadioButton.setEnabled(false);

        String text = "";
        int size = Line.mVisableRouls.size();
        for (int i = 0; i < size; i++) {
            text += Line.mVisableRouls.get(i);

            if(i != size - 1){
                text += " ";
            }
        }

        mNoViableListEditText.setText(text);

        UpdateSelectedLine(Line);
    }

    public void setLineSettingsMenuVisible(){
        mEditLineState = false;

        restartScreen();
        mLineSettingsMenu.setVisibility(View.VISIBLE);
        loadLineSettingsMenu();

        int position = mDocumentCreator.getNextPosition();

        mLineSettingsKindLineRadioGroup.setEnabled(true);
        mLineSettingsKindTableRadioButton.setEnabled(true);
        mLineSettingsKindLinearRadioButton.setEnabled(true);
        mLineSettingsKindXYRadioButton.setEnabled(true);

        mLineSettingsPositionEditText.setText(Integer.toString(position));
        mLineSettingsPaddingLeftEditText.setText(Integer.toString(0));
        mNoViableListEditText.setText("");

        return;
    }

    @Override
    public void onBackPressed() {
        Save();
    }

    public void Save(){
        try {
            String text = mDocumentCreator.GetDocument();
            File file = new File(Environment.getExternalStorageDirectory() + "/VXDocument.txt");
            if(file.exists()){
                file.delete();
            }
            file.createNewFile();

            Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "UTF-8"));

            out.write(text);
            out.flush();
            out.close();
            out.close();

            Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
        }
    }

    public void setObjectSettingsMenuVisible(){
        restartScreen();
        mObjectSettingsMenu.setVisibility(View.VISIBLE);
        loadObjectSettingsMenu();
        return;
    }

    public boolean mObjectSettingsMenuLoad = false;
    public void loadObjectSettingsMenu(){
        if(!mObjectSettingsMenuLoad) {

            if (mObjectSettingsMenu.getVisibility() == View.GONE) {
                mObjectSettingsMenu.setVisibility(View.VISIBLE);
            }

            mObjectSettingsTextMenuRelativeLayout = (RelativeLayout) findViewById(R.id.ObjectSettingsTextSettingsMenu);
            mObjectSettingsTypingMenuRelativeLayout = (RelativeLayout) findViewById(R.id.ObjectSettingsTypingMenu);
            mObjectSettingsTextValueMenuRelativeLayout = (RelativeLayout) findViewById(R.id.ObjectSettingsTextValueMenu);
            mObjectSettingsSizesMenuRelativeLayout = (RelativeLayout) findViewById(R.id.ObjectSettingsSizesMenu);
            mObjectSettingsCoordinatesMenuRelativeLayout = (RelativeLayout) findViewById(R.id.ObjectSettingsCoordinatesMenu);
            mObjectSettingsMarginsMenuRelativeLayout = (RelativeLayout) findViewById(R.id.ObjectSettingsMarginsMenu);
            mObjectSettingsBooleanValueMenuRelativeLayout = (RelativeLayout) findViewById(R.id.ObjectSettingsBooleanValueMenu);
            mObjectNameLayout = (RelativeLayout) findViewById(R.id.ObjectSettings_ID_Layout);

            mObjectSettingsNameEditText = (EditText) findViewById(R.id.ObjectSettings_NameEditText);

            mObjectSettingsTextTextEditText = (EditText) findViewById(R.id.ObjectSettings_Text_Text_EditText);
            mObjectSettingsTextTextColorEditText = (Spinner) findViewById(R.id.ObjectSettings_Text_TextColor_Spinner);
            mObjectSettingsTextSizeAutoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.ObjectSettings_Text_TextSize_EditText);

            mObjectSettingsTextBoldCheckBox = (CheckBox) findViewById(R.id.ObjectSettings_Text_Bold_CheckBox);
            mObjectSettingsTextItalicCheckBox = (CheckBox) findViewById(R.id.ObjectSettings_Text_Italic_CheckBox);

            mObjectSettingsTypingHintEditText = (EditText) findViewById(R.id.ObjectSettings_Typing_Hint_EditText);
            mObjectSettingsTypingUpperLimitEditText = (EditText) findViewById(R.id.ObjectSettings_Typing_UpperLimit_EditText);
            mObjectSettingsTypingLowerLimitEditText = (EditText) findViewById(R.id.ObjectSettings_Typing_LowerLimit_EditText);

            mObjectSettingsTypingNumbersKeyboardCheckBox = (CheckBox) findViewById(R.id.ObjectSettings_Typing_NumbersKeyboard_CheckBox);

            mObjectSettingsTextValueValueEditText = (EditText) findViewById(R.id.ObjectSettings_TextValue_Value_CheckBox);

            mObjectSettingsSizesXSizeEditText = (EditText) findViewById(R.id.ObjectSettings_Sizes_XSize_EditText);
            mObjectSettingsSizesYSizeEditText = (EditText) findViewById(R.id.ObjectSettings_Sizes_YSize_EditText);

            mObjectSettingsCoordinatesXEditText = (EditText) findViewById(R.id.ObjectSettings_Coordinates_X_EditText);
            mObjectSettingsCoordinatesYEditText = (EditText) findViewById(R.id.ObjectSettings_Coordinates_Y_EditText);

            mObjectSettingsMarginsTopEditText = (EditText) findViewById(R.id.ObjectSettings_Margins_Top_EditText);
            mObjectSettingsMarginsBottomEditText = (EditText) findViewById(R.id.ObjectSettings_Margins_Bottom_EditText);
            mObjectSettingsMarginsLeftEditText = (EditText) findViewById(R.id.ObjectSettings_Margins_Left_EditText);
            mObjectSettingsMarginsRightEditText = (EditText) findViewById(R.id.ObjectSettings_Margins_Right_EditText);

            mObjectSettingsBooleanValueValueCheckBox = (CheckBox) findViewById(R.id.ObjectSettings_BooleanValue_Value_CheckBox);

            mObjectSettingsDoneButton = (Button) findViewById(R.id.ObjectSettingsMenu_Done_Button);
            mObjectSettingsDeleteButton = (Button) findViewById(R.id.ObjectSettingsMenu_Delete_Button);

            mObjectSettingsMenuLoad = true;

            mObjectSettingsTextSizeAutoCompleteTextView.setAdapter(mTextSizeAutoCompleteCached.GetAdapter());
            mObjectSettingsTextSizeAutoCompleteTextView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(hasFocus){
                        mObjectSettingsTextSizeAutoCompleteTextView.showDropDown();
                    } else {
                        mObjectSettingsTextSizeAutoCompleteTextView.dismissDropDown();
                    }
                }
            });

        }

        return;
    }

    public boolean mLineSettingsMenuLoad = false;
    public void loadLineSettingsMenu(){
        if(!mLineSettingsMenuLoad) {

            if (mLineSettingsMenu.getVisibility() == View.GONE) {
                mLineSettingsMenu.setVisibility(View.VISIBLE);
            }

            mLineSettingsKindLineRadioGroup = (RadioGroup) findViewById(R.id.LineSettingsMenu_LineKind_RadioGroup);
            mLineSettingsKindLinearRadioButton = (RadioButton) findViewById(R.id.LineSettingsMenu_Linear_RadioButton);
            mLineSettingsKindTableRadioButton = (RadioButton) findViewById(R.id.LineSettingsMenu_Table_RadioButton);
            mLineSettingsKindXYRadioButton = (RadioButton) findViewById(R.id.LineSettingsMenu_XY_RadioButton);

            mRowsAndColumsLayout = (RelativeLayout) findViewById(R.id.RowsAndColumsLayout);
            mRowsAndColumsLayoutColumsEditText = (EditText) findViewById(R.id.LineSettingsMenu_RowsAndColums_Colums_EditText);
            mRowsAndColumsLayoutRowsEditText = (EditText) findViewById(R.id.LineSettingsMenu_RowsAndColums_Rows_EditText);

            mNoViableListEditText = (EditText) findViewById(R.id.NoViableListEditText);

            mLineSettingsPositionEditText = (EditText) findViewById(R.id.LineSettingsMenu_Position_EditText);

            mLineSettingsPaddingLeftEditText = (EditText) findViewById(R.id.LineSettingsMenu_PaddingLeft_EditText);

            mLineSettingsDoneButton = (Button) findViewById(R.id.LineSettingsMenu_Done_Button);

            mLineSettingsOrientionRadioGroup = (RadioGroup) findViewById(R.id.LineSettingsMenu_Oriention_RadioGroup);
            mLineSettingsOrientionHorizontalRadioButton = (RadioButton) findViewById(R.id.LineSettingsMenu_HorizontalRadioBox);
            mLineSettingsOrientionVerticalRadioButton = (RadioButton) findViewById(R.id.LineSettingsMenu_VerticalRadioBox);

            mLineSettingsOrientionRadioGroup.setVisibility(View.VISIBLE);

            mLineSettingsDoneButton.setOnClickListener(mLineSettingsMenuOnClickListener.get());

            RadioGroup.OnCheckedChangeListener onCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId == mLineSettingsKindLinearRadioButton.getId()) {
                        mRowsAndColumsLayout.setVisibility(View.GONE);
                        mLineSettingsOrientionRadioGroup.setVisibility(View.VISIBLE);
                        mLineSettingsOrientionRadioGroup.animate().alpha(1.0f).setListener(null);

                    } else if (checkedId == mLineSettingsKindTableRadioButton.getId()) {
                        mLineSettingsOrientionRadioGroup.setVisibility(View.GONE);
                        mRowsAndColumsLayout.setVisibility(View.VISIBLE);
                        mRowsAndColumsLayout.animate().alpha(1.0f).setListener(null);

                    } else {
                        Animator.AnimatorListener animateListener = new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                mLineSettingsOrientionRadioGroup.setVisibility(View.GONE);
                                mRowsAndColumsLayout.setVisibility(View.GONE);
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        };

                        mLineSettingsOrientionRadioGroup.animate().alpha(0.0f).setListener(animateListener);
                        mRowsAndColumsLayout.animate().alpha(0.0f).setListener(animateListener);

                    }
                }
            };

            mLineSettingsKindLineRadioGroup.setOnCheckedChangeListener(onCheckedChangeListener);

            onCheckedChangeListener.onCheckedChanged(null, mLineSettingsKindLineRadioGroup.getCheckedRadioButtonId());

            mMainMenuLoad = true;

        }

        return;
    }

    public final ThreadLocal<OnClickListener> mLineSettingsMenuOnClickListener = new ThreadLocal<OnClickListener>() {
        @Override
        protected OnClickListener initialValue() {
            return new OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (v.getId()) {
                        case R.id.LineSettingsMenu_Done_Button:
                            if(!mEditLineState) {
                                Line line = new Line();

                                switch (mLineSettingsKindLineRadioGroup.getCheckedRadioButtonId()) {
                                    case R.id.LineSettingsMenu_Linear_RadioButton:
                                        line.mLineKind = Line.LINEKIND_LINEAR;

                                        break;
                                    case R.id.LineSettingsMenu_Table_RadioButton:
                                        line.mLineKind = Line.LINEKIND_TABLE;

                                        break;
                                    case R.id.LineSettingsMenu_XY_RadioButton:
                                        line.mLineKind = Line.LINEKIND_XY;

                                        break;
                                }

                                line.mLinePosition = Integer.parseInt(mLineSettingsPositionEditText.getText().toString());
                                line.mPaddingLeft = Integer.parseInt(mLineSettingsPaddingLeftEditText.getText().toString());
                                line.mVisableRouls = getSubSpaceObjects(mNoViableListEditText.getText().toString());

                                if (line.mLineKind == Line.LINEKIND_TABLE) {
                                    try {
                                        line.mRows = Integer.parseInt(mRowsAndColumsLayoutRowsEditText.getText().toString());
                                        line.mColums = Integer.parseInt(mRowsAndColumsLayoutColumsEditText.getText().toString());

                                    } catch (NumberFormatException e) {
                                        line.mRows = 0;
                                        line.mColums = 0;
                                    }

                                } else if (line.mLineKind == Line.LINEKIND_LINEAR) {
                                    switch (mLineSettingsOrientionRadioGroup.getCheckedRadioButtonId()) {
                                        case R.id.LineSettingsMenu_HorizontalRadioBox:
                                            line.mOrientation = Line.OBJECTORIENTATION_HORIZONTAL;

                                            break;
                                        case R.id.LineSettingsMenu_VerticalRadioBox:
                                            line.mOrientation = Line.OBJECTORIENTATION_VERTICAL;

                                            break;
                                    }
                                }

                                mDocumentCreator.AddLine(line);

                                final Line finalLine = line;
                                line.mViewGroup.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        UpdateSelectedLine(finalLine);
                                    }
                                });

                                line.mViewGroup.setOnLongClickListener(new View.OnLongClickListener() {
                                    @Override
                                    public boolean onLongClick(View v) {
                                        UpdateLine(finalLine);
                                        return false;
                                    }
                                });

                                UpdateSelectedLine(line);

                                setMainMenuVisible();

                            } else {
                                mSelectedLine.mLinePosition = Integer.parseInt(mLineSettingsPositionEditText.getText().toString());
                                mSelectedLine.mPaddingLeft = Integer.parseInt(mLineSettingsPaddingLeftEditText.getText().toString());
                                mSelectedLine.mVisableRouls = getSubSpaceObjects(mNoViableListEditText.getText().toString());

                                if (mSelectedLine.mLineKind == Line.LINEKIND_TABLE) {
                                    try {
                                        mSelectedLine.mRows = Integer.parseInt(mRowsAndColumsLayoutRowsEditText.getText().toString());
                                        mSelectedLine.mColums = Integer.parseInt(mRowsAndColumsLayoutColumsEditText.getText().toString());

                                    } catch (NumberFormatException e) {
                                        mSelectedLine.mRows = 0;
                                        mSelectedLine.mColums = 0;
                                    }

                                } else if (mSelectedLine.mLineKind == Line.LINEKIND_LINEAR) {
                                    switch (mLineSettingsOrientionRadioGroup.getCheckedRadioButtonId()) {
                                        case R.id.LineSettingsMenu_HorizontalRadioBox:
                                            mSelectedLine.mOrientation = Line.OBJECTORIENTATION_HORIZONTAL;

                                            break;
                                        case R.id.LineSettingsMenu_VerticalRadioBox:
                                            mSelectedLine.mOrientation = Line.OBJECTORIENTATION_VERTICAL;

                                            break;
                                    }
                                }

                                mDocumentCreator.UpdateLine(mSelectedLine);

                                final Line finalLine = mSelectedLine;
                                mSelectedLine.mViewGroup.setOnClickListener(new OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        UpdateSelectedLine(finalLine);
                                    }
                                });

                                UpdateSelectedLine(finalLine);

                                setMainMenuVisible();
                            }

                            break;
                    }
                }
            };
        }
    };

    public void setMainMenuVisible(){
        restartScreen();
        mMainMenu.setVisibility(View.VISIBLE);
        loadMainMenu();
        return;
    }

    public boolean mMainMenuLoad = false;
    public void loadMainMenu(){
        if(!mMainMenuLoad) {

            if (mMainMenu.getVisibility() == View.GONE) {
                mMainMenu.setVisibility(View.VISIBLE);
            }

            mMainMenuAddNewLineButton = (Button) findViewById(R.id.AddNew_Line_Button);
            mMainMenuAddNewTextViewButton = (Button) findViewById(R.id.AddNew_TextView_Button);
            mMainMenuAddNewEditTextButton = (Button) findViewById(R.id.AddNew_EditText_Button);
            mMainMenuAddNewCheckBoxButton = (Button) findViewById(R.id.AddNew_CheckBox_Button);

            mMainMenuToolsEraserButton = (Button) findViewById(R.id.Tools_Eraser_Button);
            mMainMenuToolsCopyLineButton = (Button) findViewById(R.id.CopyLineButton);

            mMainMenuOptionsSaveButton = (Button) findViewById(R.id.Options_Save_Button);
            mRebuildObjectsButton = (Button) findViewById(R.id.RebuildObjectsButton);
            mShowPDFButton = (Button) findViewById(R.id.ShowPDFButton);

            mMainMenuAddNewLineButton.setOnClickListener(mMainMenuOnClickListener);
            mMainMenuAddNewTextViewButton.setOnClickListener(mMainMenuOnClickListener);
            mMainMenuAddNewEditTextButton.setOnClickListener(mMainMenuOnClickListener);
            mMainMenuAddNewCheckBoxButton.setOnClickListener(mMainMenuOnClickListener);

            mMainMenuToolsEraserButton.setOnClickListener(mMainMenuOnClickListener);

            mMainMenuOptionsSaveButton.setOnClickListener(mMainMenuOnClickListener);
            mRebuildObjectsButton.setOnClickListener(mMainMenuOnClickListener);
            mShowPDFButton.setOnClickListener(mMainMenuOnClickListener);

            mMainMenuToolsCopyLineButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mSelectedLine != null){
                        Line line = new Line();
                        line.mLineKind = mSelectedLine.mLineKind;
                        line.mLinePosition =  mDocumentCreator.getNextPosition();
                        line.mOrientation = mSelectedLine.mOrientation;
                        line.mPaddingLeft = mSelectedLine.mPaddingLeft;

                        mDocumentCreator.mJSONDocumentParser.CreateLine(line);
                        mDocumentCreator.AddLine(line);

                        line.mInLineObjects = new ArrayList<Object>();
                        for (int i = 0; i < mSelectedLine.mInLineObjects.size(); i++) {
                            Object objectToCopy = mSelectedLine.mInLineObjects.get(i);

                            Object newObject = new Object(objectToCopy);

                            switch (newObject.mObjectKind){
                                case Object.OBJECTKIND_TEXTVIEW:
                                    mDocumentCreator.mJSONDocumentParser.CreateTextView(newObject);

                                    break;

                                case Object.OBJECTKIND_EDITTEXT:
                                    mDocumentCreator.mJSONDocumentParser.CreateEditText(newObject);

                                    break;

                                case Object.OBJECTKIND_CHECKBOX:
                                    mDocumentCreator.mJSONDocumentParser.CreateCheckBox(newObject);

                                    break;
                            }

                            newObject.mObjectView.setLayoutParams(objectToCopy.mObjectView.getLayoutParams());

                            line.mInLineObjects.add(newObject);
                            line.mViewGroup.addView(newObject.mObjectView);

                            new OnLongClickOpenToEditListener(newObject, i, line);
                        }

                        UpdateSelectedLine(line);

                        final Line finalLine = line;
                        line.mViewGroup.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                UpdateSelectedLine(finalLine);
                            }
                        });

                    }
                }
            });

            mMainMenuLoad = true;

        }

        return;
    }

    public OnClickListener mMainMenuOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch(v.getId()){
                case R.id.AddNew_Line_Button:
                    setLineSettingsMenuVisible();

                    break;
                case R.id.AddNew_TextView_Button:
                    CreateObject(Object.OBJECTKIND_TEXTVIEW, mSelectedLine);

                    break;
                case R.id.AddNew_EditText_Button:
                    CreateObject(Object.OBJECTKIND_EDITTEXT, mSelectedLine);

                    break;
                case R.id.AddNew_CheckBox_Button:
                    CreateObject(Object.OBJECTKIND_CHECKBOX, mSelectedLine);

                    break;

                case R.id.Tools_Eraser_Button:

                    if(mSelectedLine != null) {
                        mDocumentCreator.getBaseLayout().removeView(mSelectedLine.mViewGroup);
                        mDocumentCreator.mLinesArray.remove(mSelectedLine);
                        mSelectedLine = null;

                    }

                    break;
                case R.id.Options_Save_Button:
                    Save();
                    break;

                case R.id.RebuildObjectsButton:
                    rebuildObjects();
                    break;

                case R.id.ShowPDFButton:
                    createPDF();
                    break;
            }
        }
    };

    public void createPDF(){
        Intent i = new Intent(this, PDFActivity.class);

        try {
            PDFActivity.mDocument = new JSONArray(mDocumentCreator.GetDocument());

            startActivity(i);
        } catch (Exception e) {
            e.printStackTrace();

            Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
        }
    }

    private void rebuildObjects(){
        for (Line line : DocumentCreator.mLinesArray) {
            for (Object mInLineObject : line.mInLineObjects) {
                mInLineObject.mObejctName = "";
            }
        }
    }

    ShapeDrawable rectShapeDrawable = new ShapeDrawable();
    Paint paint = rectShapeDrawable.getPaint();

    public void UpdateSelectedLine(Line NewLine){
        if(mSelectedLine != null){
            mSelectedLine.mViewGroup.setBackgroundDrawable(null);

            if(mSelectedLine.mVisableRouls.size() != 0){
                mSelectedLine.mViewGroup.setBackgroundColor(Color.CYAN);
            }

        }

        mSelectedLine = NewLine;

        if(mSelectedLine.mVisableRouls.size() != 0){
            paint.setColor(Color.RED);
        } else {
            paint.setColor(Color.GRAY);
        }

        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(5);

        mSelectedLine.mViewGroup.setBackgroundDrawable(rectShapeDrawable);
    }

    public void CreateObject(int ObjectKind, Line Line){
        if(Line == null){
            if(mDocumentCreator.countLines() == 0){
                Toast.makeText(MainActivity.this, "You Most Create Line Before Create Objects", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(MainActivity.this, "You Most Select Line Before Create Objects", Toast.LENGTH_SHORT).show();
            }

            return;
        }

        ObjectCreated = new Object();
        ObjectCreated.mObjectKind = ObjectKind;
        ObjectCreated.mLineKind = Line.mLineKind;

        OpenObjectsEditor(ObjectCreated, false, null, 0);

        mObjectSettingsDoneButton.setOnClickListener(OnObjectSettingsDone);
        mObjectSettingsDeleteButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                setMainMenuVisible();
            }
        });

        return;
    }

    Object ObjectCreated;
    OnClickListener OnObjectSettingsDone = new OnClickListener() {
        @Override
        public void onClick(View v) {

            GetObjectParamsTo(ObjectCreated);

            switch (ObjectCreated.mObjectKind){
                case Object.OBJECTKIND_TEXTVIEW:
                    mDocumentCreator.mJSONDocumentParser.CreateTextView(ObjectCreated);

                    break;

                case Object.OBJECTKIND_EDITTEXT:
                    mDocumentCreator.mJSONDocumentParser.CreateEditText(ObjectCreated);

                    break;

                case Object.OBJECTKIND_CHECKBOX:
                    mDocumentCreator.mJSONDocumentParser.CreateCheckBox(ObjectCreated);

                    break;
            }

            if(mSelectedLine.mLineKind == Line.LINEKIND_TABLE){
                mSelectedLine.setNewPosition(ObjectCreated.mObjectY, ObjectCreated.mObjectX);
            }

            if(mObjectSettingsTextMenuRelativeLayout.getVisibility() == View.VISIBLE){
                try {
                    mTextSizeAutoCompleteCached.EnterValue(Integer.parseInt(mObjectSettingsTextSizeAutoCompleteTextView.getText().toString()));
                } catch (NumberFormatException e){

                }
            }

            mSelectedLine.mInLineObjects.add(ObjectCreated);
            mSelectedLine.mViewGroup.addView(ObjectCreated.mObjectView);

            new OnLongClickOpenToEditListener(ObjectCreated, mSelectedLine.mInLineObjects.size() - 1, mSelectedLine);

            mLayoutScrollView.scrollTo(0, mSelectedLine.mViewGroup.getBottom() + ObjectCreated.mObjectView.getBottom());

            setMainMenuVisible();

            return;
        }
    };


    Object ObjectSelectedToEdit;
    public void OpenObjectsEditor(final Object Object, boolean Edit, final Line Line, final int Position){
        setObjectSettingsMenuVisible();

        switch (Object.mObjectKind){
            case roiapps.vxqcdocumenteditor.Object.OBJECTKIND_TEXTVIEW:
                mObjectSettingsTextMenuRelativeLayout.setVisibility(View.VISIBLE);
                mObjectSettingsTypingMenuRelativeLayout.setVisibility(View.GONE);
                mObjectSettingsSizesMenuRelativeLayout.setVisibility(View.VISIBLE);
                mObjectSettingsMarginsMenuRelativeLayout.setVisibility(View.VISIBLE);
                mObjectSettingsTextValueMenuRelativeLayout.setVisibility(View.GONE);
                mObjectSettingsBooleanValueMenuRelativeLayout.setVisibility(View.GONE);
                mObjectNameLayout.setVisibility(View.GONE);

                break;

            case roiapps.vxqcdocumenteditor.Object.OBJECTKIND_EDITTEXT:
                mObjectSettingsTextMenuRelativeLayout.setVisibility(View.GONE);
                mObjectSettingsTypingMenuRelativeLayout.setVisibility(View.VISIBLE);
                mObjectSettingsSizesMenuRelativeLayout.setVisibility(View.VISIBLE);
                mObjectSettingsMarginsMenuRelativeLayout.setVisibility(View.VISIBLE);
                mObjectSettingsTextValueMenuRelativeLayout.setVisibility(View.VISIBLE);
                mObjectSettingsBooleanValueMenuRelativeLayout.setVisibility(View.GONE);
                mObjectNameLayout.setVisibility(View.VISIBLE);

                break;

            case roiapps.vxqcdocumenteditor.Object.OBJECTKIND_CHECKBOX:
                mObjectSettingsTextMenuRelativeLayout.setVisibility(View.VISIBLE);
                mObjectSettingsTypingMenuRelativeLayout.setVisibility(View.GONE);
                mObjectSettingsSizesMenuRelativeLayout.setVisibility(View.VISIBLE);
                mObjectSettingsMarginsMenuRelativeLayout.setVisibility(View.VISIBLE);
                mObjectSettingsTextValueMenuRelativeLayout.setVisibility(View.GONE);
                mObjectSettingsBooleanValueMenuRelativeLayout.setVisibility(View.VISIBLE);
                mObjectNameLayout.setVisibility(View.VISIBLE);

                break;
        }

        if(Object.mLineKind == Line.LINEKIND_XY || Object.mLineKind == Line.LINEKIND_TABLE){
            mObjectSettingsCoordinatesMenuRelativeLayout.setVisibility(View.VISIBLE);
        } else {
            mObjectSettingsCoordinatesMenuRelativeLayout.setVisibility(View.GONE);
        }

        mTextSizeAutoCompleteCached.UpdateAdapter();
        mObjectSettingsTextTextColorEditText.setAdapter(new ArrayAdapter<String>(MainActivity.this, R.layout.support_simple_spinner_dropdown_item, DocumentCreator.mColorsNameArray));

        mObjectSettingsTextTextEditText.setText("");
        mObjectSettingsTypingHintEditText.setText("");
        mObjectSettingsTypingUpperLimitEditText.setText("");
        mObjectSettingsTypingLowerLimitEditText.setText("");
        mObjectSettingsTextBoldCheckBox.setChecked(false);
        mObjectSettingsTextItalicCheckBox.setChecked(false);
        mObjectSettingsTypingNumbersKeyboardCheckBox.setChecked(false);
        mObjectSettingsTextValueValueEditText.setText("");
        mObjectSettingsSizesXSizeEditText.setText("");
        mObjectSettingsSizesYSizeEditText.setText("");
        mObjectSettingsCoordinatesYEditText.setText("");
        mObjectSettingsCoordinatesXEditText.setText("");
        mObjectSettingsMarginsRightEditText.setText("");
        mObjectSettingsMarginsLeftEditText.setText("");
        mObjectSettingsMarginsBottomEditText.setText("");
        mObjectSettingsMarginsTopEditText.setText("");
        mObjectSettingsNameEditText.setText("");

        mObjectSettingsTextSizeAutoCompleteTextView.setText("Text");
        mObjectSettingsTextSizeAutoCompleteTextView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mObjectSettingsTextSizeAutoCompleteTextView.setOnClickListener(null);
                mObjectSettingsTextSizeAutoCompleteTextView.setText("");
            }
        });

        mObjectSettingsTextTextColorEditText.setSelection(1);

        if(Edit){
            String textSize = mTextSizeAutoCompleteCached.GetNameFor(Object.mObjectTextSize);
            if(textSize == null){
                textSize = Integer.toString(Object.mObjectTextSize);
            }

            boolean found = false;
            for (int i = 0; i < DocumentCreator.mColorValueArray.length; i++) {
                if(Object.mObjectTextColor == DocumentCreator.mColorValueArray[i]){
                    found = true;
                    mObjectSettingsTextTextColorEditText.setSelection(i);
                }
            }

            if(!found){
                mObjectSettingsTextTextColorEditText.setSelection(0);
            }

            if(Object.mObjectText != null && !Object.mObjectText.equals(Object.OBJECT_NODEFINESTRINGVALUE_CODE)) {
                mObjectSettingsTextTextEditText.setText(Object.mObjectText);
            }

            mObjectSettingsTextSizeAutoCompleteTextView.setText(textSize);
            mObjectSettingsTextBoldCheckBox.setChecked(Object.mObjectBold);
            mObjectSettingsTextItalicCheckBox.setChecked(Object.mObjectItalic);

            if(Object.mHint != null && !Object.mHint.equals(Object.OBJECT_NODEFINESTRINGVALUE_CODE)) {
                mObjectSettingsTypingHintEditText.setText(Object.mHint);
            }

            if(Object.mHaveLimits || Object.mUpperLimit != Object.mLowerLimit) {
                mObjectSettingsTypingUpperLimitEditText.setText(Double.toString(Object.mUpperLimit));
                mObjectSettingsTypingLowerLimitEditText.setText(Double.toString(Object.mLowerLimit));
            }

            mObjectSettingsTypingNumbersKeyboardCheckBox.setChecked(Object.mObjectNumbersKeyboard);

            if(Object.mTextValue != null && !Object.mTextValue.equals(Object.OBJECT_NODEFINESTRINGVALUE_CODE)) {
                mObjectSettingsTextValueValueEditText.setText(Object.mTextValue);
            }

            if(Object.mObjectYSize != Object.OBJECT_NODEFINEINTRGERVALUE_CODE){
                mObjectSettingsSizesYSizeEditText.setText(Integer.toString(Object.mObjectYSize));
            }

            if(Object.mObjectX != Object.OBJECT_NODEFINEINTRGERVALUE_CODE){
                mObjectSettingsCoordinatesXEditText.setText(Integer.toString(Object.mObjectX));
            }

            if(Object.mObjectY != Object.OBJECT_NODEFINEINTRGERVALUE_CODE){
                mObjectSettingsCoordinatesYEditText.setText(Integer.toString(Object.mObjectY));
            }

            if(Object.mObjectXSize != Object.OBJECT_NODEFINEINTRGERVALUE_CODE){
                mObjectSettingsSizesXSizeEditText.setText(Integer.toString(Object.mObjectXSize));
            }

            if(Object.mMarginTop != Object.OBJECT_NODEFINEINTRGERVALUE_CODE){
                mObjectSettingsMarginsTopEditText.setText(Integer.toString(Object.mMarginTop));
            }

            if(Object.mMarginBottom != Object.OBJECT_NODEFINEINTRGERVALUE_CODE){
                mObjectSettingsMarginsBottomEditText.setText(Integer.toString(Object.mMarginBottom));
            }

            if(Object.mMarginLeft != Object.OBJECT_NODEFINEINTRGERVALUE_CODE){
                mObjectSettingsMarginsLeftEditText.setText(Integer.toString(Object.mMarginLeft));
            }

            if(Object.mMarginRight != Object.OBJECT_NODEFINEINTRGERVALUE_CODE){
                mObjectSettingsMarginsRightEditText.setText(Integer.toString(Object.mMarginRight));
            }

            if(Object.mObejctName != null && Object.mObejctName != null && !Object.mObejctName.equals(Object.OBJECT_NODEFINESTRINGVALUE_CODE)){
                mObjectSettingsNameEditText.setText(Object.mObejctName);
            }

            mObjectSettingsBooleanValueValueCheckBox.setChecked(Object.mBooleanValue);

            ObjectSelectedToEdit = Object;
            mObjectSettingsDoneButton.setOnClickListener(AfterEditDoneClickListener);

            mObjectSettingsDeleteButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ViewGroup) Object.mObjectView.getParent()).removeView(Object.mObjectView);
                    Line.mInLineObjects.remove(Object);
                    setMainMenuVisible();
                }
            });

        } else {
            if(Object.mObjectKind == Object.OBJECTKIND_TEXTVIEW && mSelectedLine.mLineKind == Line.LINEKIND_LINEAR && mSelectedLine.mPaddingLeft == 0){
                mObjectSettingsTextSizeAutoCompleteTextView.setText("Title");
                mObjectSettingsTextTextColorEditText.setSelection(1);
                mObjectSettingsTextBoldCheckBox.setChecked(true);
            }

            if(mSelectedLine.mLineKind == Line.LINEKIND_TABLE){
                mObjectSettingsCoordinatesXEditText.setText(Integer.toString(mSelectedLine.getNextColum()));
                mObjectSettingsCoordinatesYEditText.setText(Integer.toString(mSelectedLine.getNextRow()));
                mSelectedLine.moveOne();
            }
        }
    }

    OnClickListener AfterEditDoneClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            GetObjectParamsTo(ObjectSelectedToEdit);

            switch (ObjectSelectedToEdit.mObjectKind){
                case Object.OBJECTKIND_CHECKBOX:
                    mDocumentCreator.mJSONDocumentParser.InsertObjectIntoCheckBox(ObjectSelectedToEdit);

                    break;
                case Object.OBJECTKIND_EDITTEXT:
                    mDocumentCreator.mJSONDocumentParser.InsertObjectIntoEditText(ObjectSelectedToEdit);

                    break;
                case Object.OBJECTKIND_TEXTVIEW:
                    mDocumentCreator.mJSONDocumentParser.InsertObjectIntoTextView(ObjectSelectedToEdit);

                    break;
            }

            ObjectSelectedToEdit.mObjectView.setLayoutParams(mDocumentCreator.mJSONDocumentParser.GetViewWithParams(ObjectSelectedToEdit));

            setMainMenuVisible();
        }
    };

    public ArrayList<String> getSubSpaceObjects(String Text){
        ArrayList<String> subStringsList = new ArrayList<>();

        for(int i = 0; i < Text.length();){
            int nextIndex = Text.indexOf(" ", i);

            if(nextIndex == -1){
                subStringsList.add(Text.substring(i));
                break;
            }

            String text = Text.substring(i, nextIndex);
            if(!text.equals("")) {
                subStringsList.add(text);
            }

            i = nextIndex + 1;
        }

        return subStringsList;
    }

    public void GetObjectParamsTo(Object Object){
        Object.mObjectText = mObjectSettingsTextTextEditText.getText().toString();

        int textColorPosition;
        if((textColorPosition = mObjectSettingsTextTextColorEditText.getSelectedItemPosition()) != 0) {
            Object.mObjectTextColor = DocumentCreator.mColorValueArray[textColorPosition];
        }

        String textSize;
        if(!(textSize = mObjectSettingsTextSizeAutoCompleteTextView.getText().toString()).equals("")) {
            Integer value;
            try {
                value = Integer.parseInt(textSize);
            } catch (NumberFormatException e){
                value = Object.mObjectTextSize = (Integer) mTextSizeAutoCompleteCached.GetValueFor(textSize);
            }

            Object.mObjectTextSize = value;
        }

        Object.mObjectBold = mObjectSettingsTextBoldCheckBox.isChecked();
        Object.mObjectItalic = mObjectSettingsTextItalicCheckBox.isChecked();

        String hint;
        if(!(hint = mObjectSettingsTypingHintEditText.getText().toString()).equals(""))
            Object.mHint = hint;

        boolean haveUpperLimit = false, haveLowerLimit = false;
        String upperLimit;
        if(haveUpperLimit = !(upperLimit = mObjectSettingsTypingUpperLimitEditText.getText().toString()).equals("")) {
            Object.mUpperLimit = Double.parseDouble(upperLimit);
        }

        String lowerLimit;
        if(haveLowerLimit = !(lowerLimit = mObjectSettingsTypingLowerLimitEditText.getText().toString()).equals("")) {
            Object.mLowerLimit = Double.parseDouble(lowerLimit);
        }

        if(haveUpperLimit && haveLowerLimit) {
            Object.mHaveLimits = true;
        }

        if(haveUpperLimit ^ haveLowerLimit){
            if(!haveUpperLimit) {
                mObjectSettingsTypingUpperLimitEditText.setError("You Most Enter Upper Limit And Lower Limit");
            }

            if(!haveLowerLimit) {
                mObjectSettingsTypingLowerLimitEditText.setError("You Most Enter Upper Limit And Lower Limit");
            }

            return;
        }

        Object.mObjectNumbersKeyboard = mObjectSettingsTypingNumbersKeyboardCheckBox.isChecked();

        Object.mTextValue = mObjectSettingsTextValueValueEditText.getText().toString();

        String xSize;
        if(!(xSize = mObjectSettingsSizesXSizeEditText.getText().toString()).equals("")) {
            Object.mObjectXSize = Integer.parseInt(xSize);
        }

        String ySize;
        if(!(ySize = mObjectSettingsSizesYSizeEditText.getText().toString()).equals("")) {
            Object.mObjectYSize = Integer.parseInt(ySize);
        }

        String x;
        if(!(x = mObjectSettingsCoordinatesXEditText.getText().toString()).equals("")) {
            Object.mObjectX = Integer.parseInt(x);

        } else if(Object.mLineKind == Line.LINEKIND_TABLE || Object.mLineKind == Line.LINEKIND_XY){
            mObjectSettingsCoordinatesXEditText.setError("You Most Enter This Value");

            return;
        }

        String y;
        if(!(y = mObjectSettingsCoordinatesYEditText.getText().toString()).equals("")) {
            Object.mObjectY = Integer.parseInt(y);

        } else if(Object.mLineKind == Line.LINEKIND_TABLE || Object.mLineKind == Line.LINEKIND_XY){
            mObjectSettingsCoordinatesYEditText.setError("You Most Enter This Value");

            return;
        }

        String top;
        if(!(top = mObjectSettingsMarginsTopEditText.getText().toString()).equals("")) {
            Object.mMarginTop = Integer.parseInt(top);
        }

        String bottom;
        if(!(bottom = mObjectSettingsMarginsBottomEditText.getText().toString()).equals("")) {
            Object.mMarginBottom = Integer.parseInt(bottom);
        }

        if(Object.mObjectKind == Object.OBJECTKIND_CHECKBOX && Object.mMarginBottom <= 0){
            Object.mMarginBottom = 5;
        }

        String left;
        if(!(left = mObjectSettingsMarginsLeftEditText.getText().toString()).equals("")) {
            Object.mMarginLeft = Integer.parseInt(left);
        }

        String right;
        if(!(right = mObjectSettingsMarginsRightEditText.getText().toString()).equals("")) {
            Object.mMarginRight = Integer.parseInt(right);
        }

        Object.mBooleanValue = mObjectSettingsBooleanValueValueCheckBox.isChecked();

        String name;
        if(!(name = mObjectSettingsNameEditText.getText().toString()).equals("")){
            Object.mObejctName = name;
        }

        return;
    }

    class OnLongClickOpenToEditListener implements View.OnLongClickListener {

        Object mObject;

        int mPosition;
        Line mLine;

        public OnLongClickOpenToEditListener(Object Object, int Position, Line Line){
            mObject = Object;
            mObject.mObjectView.setOnLongClickListener(this);
            mLine = Line;
            mPosition = Position;

            return;
        }

        @Override
        public boolean onLongClick(View v) {
            OpenObjectsEditor(mObject, true, mLine, mPosition);

            return false;
        }
    }

    public class AutoCompleteCached<E> {

        private NameAndValue[] mBasicItems;

        private ArrayList<NameAndValue> mValuesEnteredList = new ArrayList<NameAndValue>();

        private ArrayAdapter<String> mArrayAdapter;

        private ArrayList<String> mItemsList = new ArrayList<String>();

        public String GetNameFor(E value){
            for (NameAndValue mBasicItem : mBasicItems) {
                if(mBasicItem.mValue.equals(value)){
                    return mBasicItem.mName;
                }
            }

            return null;
        }

        public E GetValueFor(String Key){
            for (NameAndValue mBasicItem : mBasicItems) {
                if(mBasicItem.mName.equals(Key)){
                    return (E) mBasicItem.mValue;
                }
            }

            return null;
        }

        public AutoCompleteCached(Context Context, NameAndValue<E>[] BasicItems){
            mBasicItems = BasicItems;

            mArrayAdapter = new ArrayAdapter<String>(Context, R.layout.drop_down_list_item, R.id.textView, mItemsList);

            return;
        }

        public void UpdateAdapter() {
            mItemsList.clear();

            for (NameAndValue nameAndValue : mValuesEnteredList) {
                mItemsList.add(nameAndValue.mName);
            }

            for (NameAndValue nameAndValue : mBasicItems) {
                mItemsList.add(nameAndValue.mName);
            }

            mArrayAdapter.notifyDataSetChanged();

            return;
        }

        public ArrayAdapter<String> GetAdapter(){
            return mArrayAdapter;
        }

        public void EnterValue(E Value){
            boolean found = false;
            for (NameAndValue nameAndValue : mValuesEnteredList) {
                if(nameAndValue.mValue.equals(Value)){
                    found = true;
                    break;
                }
            }

            if(!found){
                mValuesEnteredList.add(new NameAndValue(Value.toString(), Value));
            }
        }
    }

    public class NameAndValue<E> {
        public String mName;
        public E mValue;

        public NameAndValue(){
            return;
        }

        public NameAndValue(String Name, E Value){
            mName = Name;
            mValue = Value;

            return;
        }

    }

}
