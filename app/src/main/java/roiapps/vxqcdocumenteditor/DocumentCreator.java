package roiapps.vxqcdocumenteditor;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Random;

public class DocumentCreator {

    private Activity mContext;

    private LinearLayout mBaseLayout;

    public static ArrayList<Line> mLinesArray;

    public JSONDocumentParser mJSONDocumentParser;

    public static int[] mColorValueArray    = { -1       , 0xff000000, 0xff888888, 0xff444444,  0xffcccccc,   0xffff0000, 0xff00ff00, 0xff0000ff, 0xffffff00 };
    public static String[] mColorsNameArray = { "Default", "Black",    "Gray",     "Dark Gray", "Light Gray", "Red",      "Green",    "Blue",     "Yellow" };

    public DocumentCreator(Activity Activity){
        mContext = Activity;
        mBaseLayout = new LinearLayout(mContext);
        mLinesArray = new ArrayList<Line>();
        mJSONDocumentParser = new JSONDocumentParser(mContext);

        mBaseLayout.setOrientation(LinearLayout.VERTICAL);
        mBaseLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
    }

    public String GetDocument() throws Exception {
        JSONArray linesArray = new JSONArray();

        for (Line line : mLinesArray) {
            JSONObject thisLine = new JSONObject();

            thisLine.put(Line.JSONOBJECT_LINE_LINEKIND_KEY, line.mLineKind);
            thisLine.put(Line.JSONOBJECT_LINE_OBJECTLINEPOSITIONAUTO_KEY, line.mLinePosition = ((ViewGroup) line.mViewGroup.getParent()).indexOfChild(line.mViewGroup));

            if(line.mOrientation == Line.OBJECTORIENTATION_HORIZONTAL || line.mOrientation == Line.OBJECTORIENTATION_VERTICAL) {
                thisLine.put(Line.JSONOBJECT_LINE_OBJECTORIENTATION_KEY, line.mOrientation);
            }

            if(line.mPaddingLeft != Line.INTEGERNOVALUE && line.mPaddingLeft > 0) {
                thisLine.put(Line.JSONOBJECT_LINE_PADDINGLEFT_KEY, line.mPaddingLeft);
            }

            int size;
            if(line.mVisableRouls != null && (size = line.mVisableRouls.size()) > 0){
                JSONArray jsonArray = new JSONArray();

                for (int i = 0; i < size; i++) {
                    jsonArray.put(line.mVisableRouls.get(i));
                }

                thisLine.put(Line.JSONOBJECT_LINE_OBJECTLINESHOWONPRODUCTARRAY_KEY, jsonArray);
            }

            JSONArray objects = new JSONArray();

            for (Object mInLineObject : line.mInLineObjects) {
                JSONObject object = new JSONObject();

                object.put(Object.JSONOBJECT_OBJECTKIND_KEY, mInLineObject.mObjectKind);

                if(mInLineObject.mObejctName != null && !mInLineObject.mObejctName.equals(Object.OBJECT_NODEFINESTRINGVALUE_CODE)) {
                    object.put(Object.JSONOBJECT_OBJECTNAME_KEY, mInLineObject.mObejctName);
                }

                if(mInLineObject.mObjectXSize != Object.OBJECT_NODEFINEINTRGERVALUE_CODE) {
                    object.put(Object.JSONOBJECT_OBJECTXSIZE_KEY, mInLineObject.mObjectXSize);
                }

                if(mInLineObject.mObjectYSize != Object.OBJECT_NODEFINEINTRGERVALUE_CODE) {
                    object.put(Object.JSONOBJECT_OBJECTYSIZE_KEY, mInLineObject.mObjectYSize);
                }

                if(mInLineObject.mObjectX != Object.OBJECT_NODEFINEINTRGERVALUE_CODE) {
                    object.put(Object.JSONOBJECT_OBJECTX_KEY, mInLineObject.mObjectX);
                }

                if(mInLineObject.mObjectY != Object.OBJECT_NODEFINEINTRGERVALUE_CODE) {
                    object.put(Object.JSONOBJECT_OBJECTY_KEY, mInLineObject.mObjectY);
                }

                if(mInLineObject.mObjectText != null && !mInLineObject.mObjectText.equals(Object.OBJECT_NODEFINESTRINGVALUE_CODE)){
                    object.put(Object.JSONOBJECT_OBJECTTEXT_KEY, mInLineObject.mObjectText);
                }

                if(mInLineObject.mObjectTextSize != Object.OBJECT_NODEFINEINTRGERVALUE_CODE){
                    object.put(Object.JSONOBJECT_OBJECTTEXTSIZE_KEY, mInLineObject.mObjectTextSize);
                }

                if(mInLineObject.mObjectTextColor != Object.OBJECT_NODEFINEINTRGERVALUE_CODE){
                    object.put(Object.JSONOBJECT_OBJECTTEXTCOLOR_KEY, mInLineObject.mObjectTextColor);
                }

                if(mInLineObject.mObjectItalic){
                    object.put(Object.JSONOBJECT_OBJECTITALIC_KEY, mInLineObject.mObjectItalic);
                }

                if(mInLineObject.mObjectBold){
                    object.put(Object.JSONOBJECT_OBJECTBOLD_KEY, mInLineObject.mObjectBold);
                }

                if(mInLineObject.mObjectImageBase64 != null && mInLineObject.mObjectImageBase64 != null){
                    if(!mInLineObject.mObjectImageBase64.equals(Object.OBJECT_NODEFINESTRINGVALUE_CODE)) {
                        object.put(Object.JSONOBJECT_OBJECTIMAGEBASE64_KEY, mInLineObject.mObjectImageBase64);
                    }
                }

                if(mInLineObject.mObjectNumbersKeyboard){
                    object.put(Object.JSONOBJECT_OBJECTNUMBERKEYBOARD_KEY, mInLineObject.mObjectNumbersKeyboard);
                }

                object.put(Object.JSONOBJECT_OBJECTMARGINLEFT_KEY, mInLineObject.mMarginLeft);
                object.put(Object.JSONOBJECT_OBJECTMARGINREIGHT_KEY, mInLineObject.mMarginRight);
                object.put(Object.JSONOBJECT_OBJECTMARGINTOP_KEY, mInLineObject.mMarginTop);
                object.put(Object.JSONOBJECT_OBJECTMARGINBOTTOM_KEY, mInLineObject.mMarginBottom);

                if(mInLineObject.mUpperLimit != Object.OBJECT_NODEFINEDOUBLEVALUE_CODE){
                    object.put(Object.JSONOBJECT_OBJECTUPPERKIMIT_KEY, mInLineObject.mUpperLimit);
                }

                if(mInLineObject.mLowerLimit != Object.OBJECT_NODEFINEDOUBLEVALUE_CODE){
                    object.put(Object.JSONOBJECT_OBJECTLOWERLIMIT_KEY, mInLineObject.mLowerLimit);
                }

                if(mInLineObject.mBooleanValue){
                    object.put(Object.JSONOBJECT_OBJECTBOOLEANVALUE_KEY, mInLineObject.mBooleanValue);
                }

                if(mInLineObject.mTextValue != null && !mInLineObject.mTextValue.equals("") && !mInLineObject.mTextValue.equals(Object.OBJECT_NODEFINESTRINGVALUE_CODE)){
                    object.put(Object.JSONOBJECT_OBJECTSTRINGVALUE_KEY, mInLineObject.mTextValue);
                }

                if(mInLineObject.mHint != null && !mInLineObject.mHint.equals(Object.OBJECT_NODEFINESTRINGVALUE_CODE)){
                    object.put(Object.JSONOBJECT_OBJECTHINT_KEY, mInLineObject.mHint);
                }

                if(mInLineObject.mHaveLimits || mInLineObject.mUpperLimit != mInLineObject.mLowerLimit){
                    object.put(Object.JSONOBJECT_OBJECTHAVELIMITS_KEY, mInLineObject.mHaveLimits);

                }

                objects.put(object);
            }

            thisLine.put(Line.JSONOBJECT_LINE_OBJECTSARRAY_KEY, objects);
            linesArray.put(thisLine);
        }

        return linesArray.toString();
    }

    public int countLines(){
        return mLinesArray.size();
    }

    public void AddLine(Line Line){
        ViewGroup thisLine = mJSONDocumentParser.CreateLine(Line);

        thisLine.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        mBaseLayout.addView(thisLine, Line.mLinePosition);
        mLinesArray.add(Line.mLinePosition, Line);

        return;
    }

    public void UpdateLine(Line Line){
        mJSONDocumentParser.UpdateLine(Line);

        mLinesArray.remove(Line);
        mBaseLayout.removeView(Line.mViewGroup);

        mBaseLayout.addView(Line.mViewGroup, Line.mLinePosition);
        mLinesArray.add(Line.mLinePosition, Line);

        return;
    }

    public LinearLayout getBaseLayout(){
        return mBaseLayout;
    }

    public int getNextPosition(){
        return mBaseLayout.getChildCount();
    }

    public int getPositionOfView(ViewGroup Parent, View Child) {
        int childId = Child.getId();

        for(int i = 0; i < Parent.getChildCount(); i++){
            if(Parent.getChildAt(i).getId() == childId){
                return i;
            }
        }

        return 0;
    }


}
