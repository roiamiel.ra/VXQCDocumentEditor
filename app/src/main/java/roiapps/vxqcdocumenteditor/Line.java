package roiapps.vxqcdocumenteditor;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.ViewGroup;

import java.util.ArrayList;

public class Line {

    public static final int OBJECTORIENTATION_VERTICAL = 1;
    public static final int OBJECTORIENTATION_HORIZONTAL = 2;

    public static final int OBJECTLINEPOSITIONAUTO = -1;

    public static final String JSONOBJECT_LINE_LINEKIND_KEY = "LineKind";
    public static final String JSONOBJECT_LINE_PADDINGLEFT_KEY = "PaddingLeft";
    public static final String JSONOBJECT_LINE_OBJECTSARRAY_KEY = "ObjectsArray";
    public static final String JSONOBJECT_LINE_OBJECTORIENTATION_KEY = "Orientation";
    public static final String JSONOBJECT_LINE_OBJECTLINEPOSITIONAUTO_KEY = "Position";
    public static final String JSONOBJECT_LINE_OBJECTLINESHOWONPRODUCTARRAY_KEY = "ShowOnProductArray";

    public static final int LINEKIND_LINEAR = 1;
    public static final int LINEKIND_TABLE = 2;
    public static final int LINEKIND_XY = 3;

    public static final int INTEGERNOVALUE = -1;

    @Nullable
    public ViewGroup mViewGroup = null;
    @NonNull
    public ArrayList<Object> mInLineObjects = new ArrayList<>();
    public int mLineKind = LINEKIND_LINEAR;
    public int mPaddingLeft = 0;
    public int mLinePosition = OBJECTLINEPOSITIONAUTO;

    public int mOrientation = OBJECTORIENTATION_VERTICAL;

    public ArrayList<String> mVisableRouls = new ArrayList<>();

    public int mRows = OBJECTLINEPOSITIONAUTO;
    public int mColums = OBJECTLINEPOSITIONAUTO;

    private int mRowsCounter = 0;
    private int mColumsCounter = 0;

    private boolean mStopMove = false;

    public int getNextRow(){
        if(mStopMove){
            return 0;
        }

        return mRowsCounter;
    }

    public int getNextColum(){
        if(mStopMove){
            return 0;
        }

        return mColumsCounter;
    }

    public void moveOne(){
        if(mStopMove){
            return;
        } else if(mColumsCounter >= mColums && mRowsCounter >= mRows){
            mColumsCounter = 0;
            mRowsCounter = 0;
            mStopMove = true;

        } else if(mColumsCounter >= mColums){
            mColumsCounter = 0;
            mRowsCounter++;

        } else {
            mColumsCounter++;

        }

        return;
    }

    public void setNewPosition(int Row, int Colum){
        if(Row < 0){
            return;
        }

        if(Colum < 0){
            return;
        }

        if(Row > mRows){
            mRows = Row;
        }

        if(Colum > mColums){
            mColums = Colum;
        }

        mRowsCounter = Row;
        mColumsCounter = Colum;

        moveOne();
    }

}