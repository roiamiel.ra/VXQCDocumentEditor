package roiapps.vxqcdocumenteditor;

import android.animation.Animator;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.widget.RelativeLayout;

import javax.xml.datatype.Duration;

public class RelativeLayoutAnimate extends RelativeLayout {

    public RelativeLayoutAnimate(Context context) {
        super(context);
    }

    public RelativeLayoutAnimate(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setVisibility(final int visibility) {
        if(visibility == View.GONE){
            Animator.AnimatorListener animationListener = new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    SetVisibility(visibility);
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            };

            animate().alpha(0.0f).setListener(animationListener);
        } else {
            Animator.AnimatorListener animationListener = new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    SetVisibility(visibility);
                }

                @Override
                public void onAnimationEnd(Animator animation) {

                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            };
            animate().alpha(1.0f).setDuration(500).setListener(animationListener);
        }
    }

    public void SetVisibility(int visibility){
        super.setVisibility(visibility);
    }
}