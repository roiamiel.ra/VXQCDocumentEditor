package roiapps.vxqcdocumenteditor;

import java.util.Date;

public class Failure extends DynamicObject {

    public Failure(){
        mFailureSource = new FailureSource();

        return;
    }

    public Failure(FailureSource FailureSource){
        mFailureSource = FailureSource;

        return;
    }

    public FailureSource mFailureSource;

    public Date mStartTime;
    public Date mEndTime;
    public String mTotalTime;
    public Date mStartWorkTime;
    public Date mEndWorkTime;
    public String mTotalWorkTime;

    public String mDescription;
    public String mCorrectiveAction;
    public String mTechnician;

    public boolean mApproval;
}
