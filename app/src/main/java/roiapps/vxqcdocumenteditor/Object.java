package roiapps.vxqcdocumenteditor;

import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONObject;

public class Object {

    /*****************************************************************************/

    public static final String JSONOBJECT_ID_KEY = "ObjectID";
    public static final String JSONOBJECT_OBJECTNAME_KEY = "ObjectName";
    public static final String JSONOBJECT_OBJECTKIND_KEY = "ObjectKind";

    public static final String JSONOBJECT_OBJECTXSIZE_KEY = "ObjectXSIZE";
    public static final String JSONOBJECT_OBJECTYSIZE_KEY = "ObjectYSIZE";
    public static final String JSONOBJECT_OBJECTX_KEY = "ObjectX";
    public static final String JSONOBJECT_OBJECTY_KEY = "ObjectY";

    public static final String JSONOBJECT_OBJECTTEXT_KEY = "ObjectText";
    public static final String JSONOBJECT_OBJECTTEXTSIZE_KEY = "ObjectTextSize";
    public static final String JSONOBJECT_OBJECTTEXTCOLOR_KEY = "ObjectTextColor";
    public static final String JSONOBJECT_OBJECTITALIC_KEY = "ObjectItalic";
    public static final String JSONOBJECT_OBJECTBOLD_KEY = "ObjectBold";

    public static final String JSONOBJECT_OBJECTIMAGEBASE64_KEY = "ObjectImageBase64";

    public static final String JSONOBJECT_OBJECTNUMBERKEYBOARD_KEY = "ObjectNumberKeyboard";

    public static final String JSONOBJECT_OBJECTMARGINLEFT_KEY = "ObjectMarinLeft";
    public static final String JSONOBJECT_OBJECTMARGINREIGHT_KEY = "ObjectMarginRight";
    public static final String JSONOBJECT_OBJECTMARGINTOP_KEY = "ObjectMarginTop";
    public static final String JSONOBJECT_OBJECTMARGINBOTTOM_KEY = "ObjectMarginBottom";

    public static final String JSONOBJECT_OBJECTSTRINGVALUE_KEY = "ObjectStringValue";
    public static final String JSONOBJECT_OBJECTBOOLEANVALUE_KEY = "ObjectBooleanValue";

    public static final String JSONOBJECT_OBJECTHINT_KEY = "ObjectHint";
    public static final String JSONOBJECT_OBJECTUPPERKIMIT_KEY = "ObjectUpperLimit";
    public static final String JSONOBJECT_OBJECTLOWERLIMIT_KEY = "ObjectLowerLimit";
    public static final String JSONOBJECT_OBJECTHAVELIMITS_KEY = "ObjectHaveLimits";

    /*****************************************************************************/

    public static final int OBJECTKIND_TEXTVIEW = 1;
    public static final int OBJECTKIND_EDITTEXT = 2;
    public static final int OBJECTKIND_CHECKBOX = 3;
    public static final int OBJECTKIND_IMAGEVIEW = 4;

    public static final int OBJECT_NODEFINEINTRGERVALUE_CODE = -1;
    public static final double OBJECT_NODEFINEDOUBLEVALUE_CODE = -1;
    public static final String OBJECT_NODEFINESTRINGVALUE_CODE = "NoDefine";

    /*****************************************************************************/

    public Object(){

    }

    public Object(Object Object){
        mLineKind = Object.mLineKind;
        mJSONObject = Object.mJSONObject;

        mLineKind = Object.mLineKind;
        mObejctName = Object.mObejctName;
        mObjectKind = Object.mObjectKind;
        mObjectXSize = Object.mObjectXSize;
        mObjectXSize = Object.mObjectYSize;

        mObjectX = Object.mObjectX;
        mObjectY = Object.mObjectY;
        mObjectText = Object.mObjectText;
        mObjectTextSize = Object.mObjectTextSize;
        mObjectTextColor = Object.mObjectTextColor;
        mObjectItalic = Object.mObjectItalic;
        mObjectBold = Object.mObjectBold;

        mObjectImageBase64 = Object.mObjectImageBase64;
        mObjectNumbersKeyboard = Object.mObjectNumbersKeyboard;

        mMarginLeft = Object.mMarginLeft;
        mMarginRight = Object.mMarginRight;
        mMarginTop = Object.mMarginTop;
        mMarginBottom = Object.mMarginBottom;

        mTextValue = Object.mTextValue;
        mBooleanValue = Object.mBooleanValue;
        mHint = Object.mHint;
        mHaveLimits = Object.mHaveLimits;
        mUpperLimit = Object.mUpperLimit;
        mLowerLimit = Object.mLowerLimit;

        return;

    }

    public View mObjectView;
    public ViewGroup mLineViewGroup;

    public int mLineKind = Line.LINEKIND_LINEAR;

    public JSONObject mJSONObject;

    public int mObjectID = 0;
    @Nullable
    public String mObejctName = OBJECT_NODEFINESTRINGVALUE_CODE;
    public int mObjectKind = OBJECTKIND_TEXTVIEW;
    public int mObjectXSize = OBJECT_NODEFINEINTRGERVALUE_CODE;
    public int mObjectYSize = OBJECT_NODEFINEINTRGERVALUE_CODE;
    public int mObjectX = OBJECT_NODEFINEINTRGERVALUE_CODE;
    public int mObjectY = OBJECT_NODEFINEINTRGERVALUE_CODE;

    @Nullable
    public String mObjectText = OBJECT_NODEFINESTRINGVALUE_CODE;
    public int mObjectTextSize = OBJECT_NODEFINEINTRGERVALUE_CODE;
    public int mObjectTextColor = OBJECT_NODEFINEINTRGERVALUE_CODE;
    public boolean mObjectItalic = false;
    public boolean mObjectBold = false;

    @Nullable
    public String mObjectImageBase64 = null;

    public boolean mObjectNumbersKeyboard = false;

    public int mMarginLeft = OBJECT_NODEFINEINTRGERVALUE_CODE;
    public int mMarginRight = OBJECT_NODEFINEINTRGERVALUE_CODE;
    public int mMarginTop = OBJECT_NODEFINEINTRGERVALUE_CODE;
    public int mMarginBottom = OBJECT_NODEFINEINTRGERVALUE_CODE;

    @Nullable
    public String mTextValue = OBJECT_NODEFINESTRINGVALUE_CODE;
    public boolean mBooleanValue = false;

    @Nullable
    public String mHint = OBJECT_NODEFINESTRINGVALUE_CODE;
    public boolean mHaveLimits = false;
    public double mUpperLimit = OBJECT_NODEFINEDOUBLEVALUE_CODE;
    public double mLowerLimit = OBJECT_NODEFINEDOUBLEVALUE_CODE;

    public boolean mHasErrorMarking = false;
    public int mColorBeforeMarking = OBJECT_NODEFINEINTRGERVALUE_CODE;

    /*****************************************************************************/
}