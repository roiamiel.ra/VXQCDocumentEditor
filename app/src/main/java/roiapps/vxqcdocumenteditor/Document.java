package roiapps.vxqcdocumenteditor;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Date;

public class Document extends DynamicObject {
    public Document(){
        mDocumentSource = new DocumentSource();

        return;
    }

    public Document(@NonNull DocumentSource DocumentSource){
        mDocumentSource = new DocumentSource();

        mDocumentSource.mProduct = DocumentSource.mProduct;
        mDocumentSource.mType = DocumentSource.mType;
        mDocumentSource.mRevision = DocumentSource.mRevision;
        mDocumentSource.mImage = DocumentSource.mImage;
        mDocumentSource.mID = DocumentSource.mID;
        mDocumentSource.mAvailable = DocumentSource.mAvailable;

        mDocumentSource.mIdentificationList = new ArrayList<Identification>();
        for (int i = 0; i < DocumentSource.mIdentificationList.size(); i++) {
            Identification identification = DocumentSource.mIdentificationList.get(i);

            Identification newIdentification = new Identification();
            newIdentification.mUnique = identification.mUnique;
            newIdentification.mName = identification.mName;
            newIdentification.mValue = identification.mValue;

            mDocumentSource.mIdentificationList.add(newIdentification);
        }

        mSourceDocumentID = DocumentSource.mID;

        return;
    }

    public DocumentSource mDocumentSource;

    public int mSourceDocumentID;

    @Nullable
    public Date mStartTime;
    @Nullable
    public Date mEndTime;
    public String mTotalTime;

    public String mTester;

    @Nullable
    public JSONArray mDocumentJSONArray;

    @NonNull
    public ArrayList<Failure> mFailuresList = new ArrayList<Failure>();
    public JSONArray mFailuresJSONArray;
    public String mFailuresQAL;

    public String mQALEditTexts;
    public String mQALCheckBoxs;

    public String mHMEditTexts;
    public String mHMCheckBoxs;

    public Bitmap mSignatrue;

}
