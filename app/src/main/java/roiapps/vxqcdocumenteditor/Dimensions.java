package roiapps.vxqcdocumenteditor;

import android.util.TypedValue;

public abstract class Dimensions {
    public static float milsToPx(float Mils){
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_IN, Mils / 1000, App.mDisplayMetrics);
    }

    public static float mmToPx(float Mm){
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_MM, Mm, App.mDisplayMetrics);
    }

    /*

    public static float dpiToPx(float Dpi){
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, Dpi, App.mDisplayMetrics);
    }

    public static float dpToPx(int Dp) {
        return Dp * (App.mDisplayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    */

}
