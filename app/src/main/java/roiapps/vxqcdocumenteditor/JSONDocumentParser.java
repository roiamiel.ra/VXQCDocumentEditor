package roiapps.vxqcdocumenteditor;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Looper;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import static roiapps.vxqcdocumenteditor.Line.LINEKIND_LINEAR;
import static roiapps.vxqcdocumenteditor.Line.LINEKIND_TABLE;
import static roiapps.vxqcdocumenteditor.Line.LINEKIND_XY;


/**
 * version 0.7 (beta)
 * author Roi
 */
public class JSONDocumentParser {

    public static final String mStatisticTab = "░";

    private Activity mContext;

    private int mMarkErrorColor = Color.RED;

    public JSONDocumentParser(Activity Activity){
        mContext = Activity;

        return;
    }

    /*****************************************************************************/

    //Put Values

    
    public synchronized LinearLayout PutLineToLayout(final DocumentStructure DocumentStructure, LinearLayout LinearLayout){
        return PutLineToLayout(DocumentStructure, LinearLayout, mDefaultOnAddLine, mDefaultOnAddObject);
    }

    
    public synchronized LinearLayout PutLineToLayout(final DocumentStructure DocumentStructure, LinearLayout LinearLayout, OnAddLine OnAddLine, OnAddObject OnAddObject){
        LinearLayout.setOrientation(LinearLayout.VERTICAL);

        for(int i = 0; i < DocumentStructure.mLineArrayList.size(); i++){
            Line line = DocumentStructure.mLineArrayList.get(i);

            if(line == null){
                continue;
            }

            final ViewGroup lineLayoutViewGroup = CreateLine(line);
            line.mViewGroup = lineLayoutViewGroup;

            if(line.mVisableRouls.size() > 0){
                line.mViewGroup.setBackgroundColor(Color.CYAN);
            }

            for(int j = 0; j < line.mInLineObjects.size(); j++){
                Object object = line.mInLineObjects.get(j);

                if(object == null){
                    continue;
                }

                object.mLineViewGroup = lineLayoutViewGroup;

                switch (object.mObjectKind){
                    case Object.OBJECTKIND_TEXTVIEW:
                        CreateTextView(object);
                        break;

                    case Object.OBJECTKIND_CHECKBOX:
                        CreateCheckBox(object);
                        break;

                    case Object.OBJECTKIND_EDITTEXT:
                        CreateEditText(object);
                        break;

                    case Object.OBJECTKIND_IMAGEVIEW:
                        CreateImageView(object);
                        break;

                    default:
                        throw new RuntimeException("ObjectKind of object number: " + j + " In Line Number: " + i + " Has Undefine value");
                }

                OnAddObject.OnAddObject(lineLayoutViewGroup, object, line);
            }

            OnAddLine.OnAddLine(LinearLayout, line);
        }

        return LinearLayout;
    }

    private OnAddLine mDefaultOnAddLine = new OnAddLine(){
        @Override
        public void OnAddLine(LinearLayout AddTo, Line Line) {
            AddTo.addView(Line.mViewGroup, Line.mLinePosition);
        }
    };

    public interface OnAddLine {
        public void OnAddLine(LinearLayout AddTo, Line Line);
    }

    private OnAddObject mDefaultOnAddObject = new OnAddObject(){
        @Override
        public void OnAddObject(ViewGroup AddTo, Object Object, Line Line) {
            AddTo.addView(Object.mObjectView);
        }
    };

    public interface OnAddObject {
        public void OnAddObject(ViewGroup AddTo, Object Object, Line Line);
    }

    /*****************************************************************************/


    //Create Line

   
    public synchronized ViewGroup CreateLine(final Line Line){
        ViewGroup lineLayoutViewGroup = null;

        switch(Line.mLineKind){
            case LINEKIND_LINEAR:
                lineLayoutViewGroup = new LinearLayout(mContext);

                ((LinearLayout) lineLayoutViewGroup).setOrientation(Line.mOrientation == Line.OBJECTORIENTATION_HORIZONTAL ? LinearLayout.HORIZONTAL : LinearLayout.VERTICAL);

                break;
            case LINEKIND_TABLE:
                lineLayoutViewGroup = new GridLayout(mContext);

                break;
            case LINEKIND_XY:
                lineLayoutViewGroup = new RelativeLayout(mContext);

                break;
            default:
                throw new RuntimeException("LineKind Of Some Line Has Undefine value");
        }

        final int padding = 16;

        /*
        if(Line.mPaddingLeft != Object.OBJECT_NODEFINEINTRGERVALUE_CODE){
            lineLayoutViewGroup.setPadding(padding + Line.mPaddingLeft, padding, padding, padding);
        }
        */

        lineLayoutViewGroup.setPadding(padding + Line.mPaddingLeft, padding, padding, padding);

        return Line.mViewGroup = lineLayoutViewGroup;
    }

    public synchronized void UpdateLine(final Line Line){
        final int padding = 16;

        /*
        if(Line.mPaddingLeft != Object.OBJECT_NODEFINEINTRGERVALUE_CODE){
            lineLayoutViewGroup.setPadding(padding + Line.mPaddingLeft, padding, padding, padding);
        }
        */

        Line.mViewGroup.setPadding(padding + Line.mPaddingLeft, padding, padding, padding);

        return;
    }


    /*****************************************************************************/


    //Create View Objects

    public synchronized void CreateTextView(Object Object){
        Object.mObjectView = new TextView(mContext);
        Object.mObjectView.setLayoutParams(GetViewWithParams(Object));

        InsertObjectIntoTextView(Object);

        return;
    }

    public synchronized void CreateCheckBox(Object Object){
        Object.mObjectView = new CheckBox(mContext);
        Object.mObjectView.setLayoutParams(GetViewWithParams(Object));

        InsertObjectIntoCheckBox(Object);

        return;
    }

    public synchronized void CreateEditText(Object Object){
        Object.mObjectView = new EditText(mContext);
        Object.mObjectView.setLayoutParams(GetViewWithParams(Object));

        InsertObjectIntoEditText(Object);

        return;
    }
    
    private synchronized View CreateImageView(Object Object){
        Button button = new Button(mContext);
        button.setLayoutParams(GetViewWithParams(Object));

        if(!Object.mObjectText.equals(Object.OBJECT_NODEFINESTRINGVALUE_CODE)){
            button.setText(Object.mObjectText);
        }

        if(Object.mObjectTextSize != Object.OBJECT_NODEFINEINTRGERVALUE_CODE){
            button.setTextSize(Object.mObjectTextSize);
        }

        if(Object.mObjectTextColor != Object.OBJECT_NODEFINEINTRGERVALUE_CODE){
            button.setTextColor(Object.mObjectTextColor);
        }

        if(Object.mObjectItalic && Object.mObjectBold){
            button.setTypeface(null, Typeface.BOLD_ITALIC);

        } else if(Object.mObjectItalic){
            button.setTypeface(null, Typeface.ITALIC);

        } else if(Object.mObjectBold){
            button.setTypeface(null, Typeface.BOLD);

        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Dialog dialog = new Dialog(mContext);
                dialog.setTitle("Image");
                //dialog.setContentView(R.layout.image_dialog);

                ImageView imageView = (ImageView) dialog.findViewById(R.id.imageView);

                Bitmap b = Bitmap.createBitmap(200, 200, Bitmap.Config.ARGB_8888);
                Canvas c = new Canvas(b);

                c.drawColor(Color.GREEN);

                imageView.setImageBitmap(b);

                dialog.show();
            }
        });

        return Object.mObjectView = button;
    } //TODO לטפל

    /*****************************************************************************/


    //Insert Object Into View

    public synchronized void InsertObjectIntoTextView(Object Object){

        TextView textView = (TextView) Object.mObjectView;

        if(!Object.mObjectText.equals(Object.OBJECT_NODEFINESTRINGVALUE_CODE)){
            textView.setText(Object.mObjectText);
        }

        if(Object.mObjectTextSize != Object.OBJECT_NODEFINEINTRGERVALUE_CODE){
            textView.setTextSize(Object.mObjectTextSize);
        }

        if(Object.mObjectTextColor != Object.OBJECT_NODEFINEINTRGERVALUE_CODE){
            textView.setTextColor(Object.mObjectTextColor);
        }

        if(Object.mObjectItalic && Object.mObjectBold){
            textView.setTypeface(null, Typeface.BOLD_ITALIC);

        } else if(Object.mObjectItalic){
            textView.setTypeface(null, Typeface.ITALIC);

        } else if(Object.mObjectBold){
            textView.setTypeface(null, Typeface.BOLD);

        } else {
            textView.setTypeface(null, Typeface.NORMAL);
        }

        return;
    }

    public synchronized void InsertObjectIntoCheckBox(final Object Object){
        final CheckBox checkBox = (CheckBox) Object.mObjectView;

        checkBox.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);

        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) checkBox.getLayoutParams();
        layoutParams.bottomMargin += 10;
        checkBox.setLayoutParams(layoutParams);

        if(Object.mObjectText != Object.OBJECT_NODEFINESTRINGVALUE_CODE && Object.mObjectText != null){
            checkBox.setText(Object.mObjectText);
        }

        if(Object.mObjectTextSize != Object.OBJECT_NODEFINEINTRGERVALUE_CODE){
            checkBox.setTextSize(Object.mObjectTextSize);
        }

        if(Object.mObjectTextColor != Object.OBJECT_NODEFINEINTRGERVALUE_CODE){
            checkBox.setTextColor(Object.mObjectTextColor);
        }

        if(Object.mObjectItalic && Object.mObjectBold){
            checkBox.setTypeface(null, Typeface.BOLD_ITALIC);

        } else if(Object.mObjectItalic){
            checkBox.setTypeface(null, Typeface.ITALIC);

        } else if(Object.mObjectBold){
            checkBox.setTypeface(null, Typeface.BOLD);

        } else {
            checkBox.setTypeface(null, Typeface.NORMAL);
        }

        new android.os.Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                checkBox.setChecked(Object.mBooleanValue);
            }
        });

        return;
    }

    public synchronized void InsertObjectIntoEditText(final Object Object){
        final EditText editText = (EditText) Object.mObjectView;

        editText.setMinWidth(Object.mObjectXSize != Object.OBJECT_NODEFINEINTRGERVALUE_CODE ? Object.mObjectXSize : 80);

        if(Object.mObjectTextSize != Object.OBJECT_NODEFINEINTRGERVALUE_CODE){
            editText.setTextSize(Object.mObjectTextSize);
        }

        if(Object.mObjectTextColor != Object.OBJECT_NODEFINEINTRGERVALUE_CODE){
            editText.setTextColor(Object.mObjectTextColor);
        }

        if(Object.mObjectItalic && Object.mObjectBold){
            editText.setTypeface(null, Typeface.BOLD_ITALIC);

        } else if(Object.mObjectItalic){
            editText.setTypeface(null, Typeface.ITALIC);

        } else if(Object.mObjectBold){
            editText.setTypeface(null, Typeface.BOLD);

        } else {
            editText.setTypeface(null, Typeface.NORMAL);
        }

        if(Object.mTextValue != Object.OBJECT_NODEFINESTRINGVALUE_CODE && Object.mTextValue != null){
            editText.setText(Object.mTextValue);

        }

        if(Object.mHint != Object.OBJECT_NODEFINESTRINGVALUE_CODE && Object.mHint != null){
            editText.setHint(Object.mHint);

        }

        if(Object.mObjectNumbersKeyboard){
            editText.setInputType(InputType.TYPE_CLASS_PHONE);
        }

        if(Object.mHaveLimits || Object.mUpperLimit != Object.mLowerLimit){
            TextWatcher textWatcher = new TextWatcher() {
                final EditText ThisEditText = editText;

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if(s.length() != 0) {
                        GetEditTextToleranceExceed(Object, ThisEditText.getText().toString());
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            };
            editText.addTextChangedListener(textWatcher);

        }

        return;
    }

    private synchronized void InsertObjectIntoImageView(Object Object){
        Button button = new Button(mContext);
        button.setLayoutParams(GetViewWithParams(Object));

        if(!Object.mObjectText.equals(Object.OBJECT_NODEFINESTRINGVALUE_CODE)){
            button.setText(Object.mObjectText);
        }

        if(Object.mObjectTextSize != Object.OBJECT_NODEFINEINTRGERVALUE_CODE){
            button.setTextSize(Object.mObjectTextSize);
        }

        if(Object.mObjectTextColor != Object.OBJECT_NODEFINEINTRGERVALUE_CODE){
            button.setTextColor(Object.mObjectTextColor);
        }

        if(Object.mObjectItalic && Object.mObjectBold){
            button.setTypeface(null, Typeface.BOLD_ITALIC);

        } else if(Object.mObjectItalic){
            button.setTypeface(null, Typeface.ITALIC);

        } else if(Object.mObjectBold){
            button.setTypeface(null, Typeface.BOLD);

        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Dialog dialog = new Dialog(mContext);
                dialog.setTitle("Image");
                //dialog.setContentView(R.layout.image_dialog);

                ImageView imageView = (ImageView) dialog.findViewById(R.id.imageView);

                Bitmap b = Bitmap.createBitmap(200, 200, Bitmap.Config.ARGB_8888);
                Canvas c = new Canvas(b);

                c.drawColor(Color.GREEN);

                imageView.setImageBitmap(b);

                dialog.show();
            }
        });

        return;
    } //TODO לטפל

   
    private synchronized String GetEditTextToleranceExceed(Object Object, String Text){
        double value = 0;

        if(Object.mHaveLimits) {
            try {
                value = Double.parseDouble(Text);

            } catch (Exception e) {
                return "ערך לא תקני, אנא הזן ערך תקני";
            }

            if (value > Object.mLowerLimit && value > Object.mUpperLimit) {
                return "הערך גבוהה מהמותר";

            } else if (value < Object.mLowerLimit && value < Object.mUpperLimit) {
                return "הערך נמוך מהמותר";

            }
        }

        return null;
    }

    /*****************************************************************************/


    //Create View Params

    
    public synchronized ViewGroup.LayoutParams GetViewWithParams(Object Object){

        switch (Object.mLineKind){
            case LINEKIND_TABLE:
                return GetGridLayoutParams(Object);

            case LINEKIND_XY:
                return GetRelativeLayoutParams(Object);

            case LINEKIND_LINEAR:
                return GetLinearLayoutParams(Object);

            default:
                throw new RuntimeException("LineKind undefine value");
        }
    }

    /*****************************************************************************/


    //Create Layout Params

    
    private synchronized ViewGroup.LayoutParams GetGridLayoutParams(Object Object){
        GridLayout.LayoutParams gridLayoutLayoutParams = new GridLayout.LayoutParams();

        gridLayoutLayoutParams.height = Object.mObjectYSize != Object.OBJECT_NODEFINEINTRGERVALUE_CODE ? Object.mObjectYSize : GridLayout .LayoutParams.WRAP_CONTENT;
        gridLayoutLayoutParams.width = Object.mObjectXSize != Object.OBJECT_NODEFINEINTRGERVALUE_CODE ? Object.mObjectXSize : GridLayout.LayoutParams.WRAP_CONTENT;

        gridLayoutLayoutParams.setMargins(
                Object.mMarginLeft,
                Object.mMarginTop,
                Object.mMarginRight,
                Object.mMarginBottom
        );

        if(Object.mObjectY == Object.OBJECT_NODEFINEINTRGERVALUE_CODE || Object.mObjectX == Object.OBJECT_NODEFINEINTRGERVALUE_CODE)
            throw new RuntimeException("In Table You Most define x and y");

        gridLayoutLayoutParams.columnSpec = GridLayout.spec(Object.mObjectX);
        gridLayoutLayoutParams.rowSpec = GridLayout.spec(Object.mObjectY);

        return gridLayoutLayoutParams;
    }

    
    private synchronized ViewGroup.LayoutParams GetRelativeLayoutParams(Object Object){
        RelativeLayout.LayoutParams relativeLayoutParams = new RelativeLayout.LayoutParams(
                (Object.mObjectXSize != Object.OBJECT_NODEFINEINTRGERVALUE_CODE ? Object.mObjectXSize : ViewGroup.LayoutParams.WRAP_CONTENT),
                (Object.mObjectYSize != Object.OBJECT_NODEFINEINTRGERVALUE_CODE ? Object.mObjectYSize : ViewGroup.LayoutParams.WRAP_CONTENT));

        relativeLayoutParams.setMargins(
                Object.mObjectX != Object.OBJECT_NODEFINEINTRGERVALUE_CODE ? Object.mObjectX : 0,
                Object.mObjectY != Object.OBJECT_NODEFINEINTRGERVALUE_CODE ? Object.mObjectY : 0,
                Object.mMarginRight,
                Object.mMarginBottom
        );

        return relativeLayoutParams;
    }

    
    private synchronized ViewGroup.LayoutParams GetLinearLayoutParams(Object Object){
        LinearLayout.LayoutParams linearLayoutParams = new LinearLayout.LayoutParams(
                (Object.mObjectXSize != Object.OBJECT_NODEFINEINTRGERVALUE_CODE ? Object.mObjectXSize : ViewGroup.LayoutParams.WRAP_CONTENT),
                (Object.mObjectYSize != Object.OBJECT_NODEFINEINTRGERVALUE_CODE ? Object.mObjectYSize : ViewGroup.LayoutParams.WRAP_CONTENT));

        linearLayoutParams.setMargins(
                Object.mMarginLeft,
                Object.mMarginTop,
                Object.mMarginRight,
                Object.mMarginBottom
        );

        return linearLayoutParams;
    }

    /*****************************************************************************/


    //Create Structure

    private synchronized int GetIfHasInt(JSONObject JSONObject, String Key, int DefualtValue) throws JSONException {
        if(JSONObject.has(Key)){
            return JSONObject.getInt(Key);
        } else {
            return DefualtValue;
        }
    }

    private synchronized String GetIfHasString(JSONObject JSONObject, String Key) throws JSONException {
        if(JSONObject.has(Key)){
            return JSONObject.getString(Key);
        } else {
            return null;
        }
    }

    private synchronized double GetIfHasDouble(JSONObject JSONObject, String Key, double DefualtValue) throws JSONException {
        if(JSONObject.has(Key)){
            return JSONObject.getDouble(Key);
        } else {
            return DefualtValue;
        }
    }

    private synchronized boolean GetIfHasBoolean(JSONObject JSONObject, String Key, boolean DefualtValue) throws JSONException {
        if(JSONObject.has(Key)){
            return JSONObject.getBoolean(Key);
        } else {
            return DefualtValue;
        }
    }

    private LinkedHashMap<String, Integer> mObjectsNames;
    
    private synchronized Object  InsertObjectValues(JSONObject thisObjectJSONObject, Line thisLineObject, JSONObject thisLineJSONObject, int objectCounter) throws JSONException {
        Object thisObject = new Object();

        thisObject.mObjectID = objectCounter;
        thisObject.mLineKind = thisLineObject.mLineKind;
        thisObject.mJSONObject = thisObjectJSONObject;

        thisObject.mObjectKind = GetIfHasInt(thisObjectJSONObject, Object.JSONOBJECT_OBJECTKIND_KEY, Object.OBJECT_NODEFINEINTRGERVALUE_CODE);

        thisObject.mObjectXSize = GetIfHasInt(thisObjectJSONObject, Object.JSONOBJECT_OBJECTXSIZE_KEY, Object.OBJECT_NODEFINEINTRGERVALUE_CODE);
        thisObject.mObjectYSize = GetIfHasInt(thisObjectJSONObject, Object.JSONOBJECT_OBJECTYSIZE_KEY, Object.OBJECT_NODEFINEINTRGERVALUE_CODE);
        thisObject.mMarginLeft = GetIfHasInt(thisObjectJSONObject, Object.JSONOBJECT_OBJECTMARGINLEFT_KEY, Object.OBJECT_NODEFINEINTRGERVALUE_CODE);
        thisObject.mMarginRight = GetIfHasInt(thisObjectJSONObject, Object.JSONOBJECT_OBJECTMARGINREIGHT_KEY, Object.OBJECT_NODEFINEINTRGERVALUE_CODE);
        thisObject.mMarginTop = GetIfHasInt(thisObjectJSONObject, Object.JSONOBJECT_OBJECTMARGINTOP_KEY, Object.OBJECT_NODEFINEINTRGERVALUE_CODE);
        thisObject.mMarginBottom = GetIfHasInt(thisObjectJSONObject, Object.JSONOBJECT_OBJECTMARGINBOTTOM_KEY, Object.OBJECT_NODEFINEINTRGERVALUE_CODE);

        if (thisObject.mLineKind == LINEKIND_XY || thisObject.mLineKind == LINEKIND_TABLE) {
            thisObject.mObjectX = GetIfHasInt(thisObjectJSONObject, Object.JSONOBJECT_OBJECTX_KEY, Object.OBJECT_NODEFINEINTRGERVALUE_CODE);
            thisObject.mObjectY = GetIfHasInt(thisObjectJSONObject, Object.JSONOBJECT_OBJECTY_KEY, Object.OBJECT_NODEFINEINTRGERVALUE_CODE);

        }

        if (thisObject.mObjectKind == Object.OBJECTKIND_TEXTVIEW || thisObject.mObjectKind == Object.OBJECTKIND_CHECKBOX || thisObject.mObjectKind == Object.OBJECTKIND_EDITTEXT) {
            thisObject.mObjectText = GetIfHasString(thisObjectJSONObject, Object.JSONOBJECT_OBJECTTEXT_KEY);
            thisObject.mObjectTextSize = GetIfHasInt(thisObjectJSONObject, Object.JSONOBJECT_OBJECTTEXTSIZE_KEY, Object.OBJECT_NODEFINEINTRGERVALUE_CODE);
            thisObject.mObjectTextColor = GetIfHasInt(thisObjectJSONObject, Object.JSONOBJECT_OBJECTTEXTCOLOR_KEY, Object.OBJECT_NODEFINEINTRGERVALUE_CODE);
            thisObject.mObjectItalic = GetIfHasBoolean(thisObjectJSONObject, Object.JSONOBJECT_OBJECTITALIC_KEY, false);
            thisObject.mObjectBold = GetIfHasBoolean(thisObjectJSONObject, Object.JSONOBJECT_OBJECTBOLD_KEY, false);

        }

        if (thisObject.mObjectKind == Object.OBJECTKIND_CHECKBOX) {
            thisObject.mBooleanValue = GetIfHasBoolean(thisObjectJSONObject, Object.JSONOBJECT_OBJECTBOOLEANVALUE_KEY, false);

        }

        if (thisObject.mObjectKind == Object.OBJECTKIND_EDITTEXT) {
            thisObject.mTextValue = GetIfHasString(thisObjectJSONObject, Object.JSONOBJECT_OBJECTSTRINGVALUE_KEY);
            thisObject.mHint = GetIfHasString(thisObjectJSONObject, Object.JSONOBJECT_OBJECTHINT_KEY);
            thisObject.mUpperLimit = GetIfHasDouble(thisObjectJSONObject, Object.JSONOBJECT_OBJECTUPPERKIMIT_KEY, Object.OBJECT_NODEFINEINTRGERVALUE_CODE);
            thisObject.mLowerLimit = GetIfHasDouble(thisObjectJSONObject, Object.JSONOBJECT_OBJECTLOWERLIMIT_KEY, Object.OBJECT_NODEFINEINTRGERVALUE_CODE);
            thisObject.mHaveLimits = GetIfHasBoolean(thisObjectJSONObject, Object.JSONOBJECT_OBJECTHAVELIMITS_KEY, false);
            thisObject.mObjectNumbersKeyboard = GetIfHasBoolean(thisObjectJSONObject, Object.JSONOBJECT_OBJECTNUMBERKEYBOARD_KEY, false);

        }

        if (thisObject.mObjectKind == Object.OBJECTKIND_EDITTEXT || thisObject.mObjectKind == Object.OBJECTKIND_CHECKBOX) {
            thisObject.mObejctName = GetIfHasString(thisObjectJSONObject, Object.JSONOBJECT_OBJECTNAME_KEY);

            if(thisObject.mObejctName == null || thisObject.mObejctName.equals("")) {

                if (thisObject.mObjectText != Object.OBJECT_NODEFINESTRINGVALUE_CODE && thisObject.mObjectText != null) {
                    int length = thisObject.mObjectText.length();
                    thisObject.mObejctName = thisObject.mObjectText.substring(0, (length < 25 ? length : 25)) + "...";

                } else if (thisObject.mHint != Object.OBJECT_NODEFINESTRINGVALUE_CODE && thisObject.mHint != null) {
                    int length = thisObject.mHint.length();
                    thisObject.mObejctName = thisObject.mHint.substring(0, (length < 25 ? length : 25)) + "...";

                } else {
                    thisObject.mObejctName = "אובייקט מספר " + Integer.toString(objectCounter) + " חסר שם";

                }
            }

            Integer has = mObjectsNames.get(thisObject.mObejctName);
            if(has != null){
                has++;
                mObjectsNames.put(thisObject.mObejctName, has);
                thisObject.mObejctName += " - " + Integer.toString(has);
                thisObject.mJSONObject.put(Object.JSONOBJECT_OBJECTNAME_KEY, thisObject.mObejctName);
            }

            thisObject.mObejctName = thisObject.mObejctName.replace("\n", " ");

            mObjectsNames.put(thisObject.mObejctName, 0);
        }

        if (thisObject.mObjectKind == Object.OBJECTKIND_IMAGEVIEW) {
            thisObject.mObjectImageBase64 = GetIfHasString(thisLineJSONObject, Object.JSONOBJECT_OBJECTIMAGEBASE64_KEY);
        }

        return thisObject;
    }

   
    public synchronized DocumentStructure GetDocumentStructure(JSONArray JSONArray){
        int size = JSONArray.length();

        ArrayList<Line> linesArrayList = new ArrayList<>(size + 1);
        ArrayList<Object> objectsArrayList = new ArrayList<>();

        mObjectsNames = new LinkedHashMap<String, Integer>();

        int objectCounter = 0;
        JSONObject thisLineJSONObject = null;
        for(int i = 0; i < size; i++){
            try {
                thisLineJSONObject = JSONArray.getJSONObject(i);

                Line thisLineObject = new Line();

                thisLineObject.mLineKind = GetIfHasInt(thisLineJSONObject, Line.JSONOBJECT_LINE_LINEKIND_KEY, Object.OBJECT_NODEFINEINTRGERVALUE_CODE);
                thisLineObject.mPaddingLeft = GetIfHasInt(thisLineJSONObject, Line.JSONOBJECT_LINE_PADDINGLEFT_KEY, Object.OBJECT_NODEFINEINTRGERVALUE_CODE);
                thisLineObject.mLinePosition = GetIfHasInt(thisLineJSONObject, Line.JSONOBJECT_LINE_OBJECTLINEPOSITIONAUTO_KEY, Object.OBJECT_NODEFINEINTRGERVALUE_CODE);

                if(thisLineJSONObject.has(Line.JSONOBJECT_LINE_OBJECTLINESHOWONPRODUCTARRAY_KEY)) {
                    JSONArray visibleObjectsJSONArray = thisLineJSONObject.getJSONArray(Line.JSONOBJECT_LINE_OBJECTLINESHOWONPRODUCTARRAY_KEY);
                    if (visibleObjectsJSONArray != null) {
                        thisLineObject.mVisableRouls.clear();
                        for (int j = 0; j < visibleObjectsJSONArray.length(); j++) {
                            String text = visibleObjectsJSONArray.getString(j);

                            if (!text.equals("")) {
                                thisLineObject.mVisableRouls.add(text);
                            }
                        }
                    }
                }

                if (thisLineObject.mLineKind == LINEKIND_LINEAR) {
                    thisLineObject.mOrientation = GetIfHasInt(thisLineJSONObject, Line.JSONOBJECT_LINE_OBJECTORIENTATION_KEY, Object.OBJECT_NODEFINEINTRGERVALUE_CODE);
                }

                JSONArray thisLineObjectsJSONArray = thisLineJSONObject.getJSONArray(Line.JSONOBJECT_LINE_OBJECTSARRAY_KEY);

                for (int j = 0; j < thisLineObjectsJSONArray.length(); j++) {

                    Object thisObject = InsertObjectValues(thisLineObjectsJSONArray.getJSONObject(j), thisLineObject, thisLineJSONObject, objectCounter);

                    objectsArrayList.add(thisObject);
                    thisLineObject.mInLineObjects.add(thisObject);

                    objectCounter++;
                }


                boolean has = false;
                try{
                    has = linesArrayList.get(thisLineObject.mLinePosition) != null;
                } catch (Exception e){
                    has = false;
                }

                if(thisLineObject.mLinePosition != Line.OBJECTLINEPOSITIONAUTO && !has){
                    int tempSize;
                    if(thisLineObject.mLinePosition >= (tempSize = linesArrayList.size())){
                        for(int j = tempSize; j < thisLineObject.mLinePosition; j++){
                            linesArrayList.add(j, null);
                        }
                    }

                    linesArrayList.add(thisLineObject.mLinePosition, thisLineObject);

                } else {
                    linesArrayList.add(thisLineObject);
                    thisLineObject.mLinePosition = linesArrayList.size() - 1;

                }

            } catch (JSONException e) {
                e.printStackTrace();
                mContext.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //AlertUIDialog alertUIDialog = new AlertUIDialog(mContext, "משהו נכשל ):", "נכל בעת קבלת אוביקט");
                        //alertUIDialog.show();
                    }
                });

                return null;
            }
        }

        DocumentStructure documentStructure = new DocumentStructure();
        documentStructure.mLineArrayList = linesArrayList;
        documentStructure.mObjectsArrayList = objectsArrayList;

        return documentStructure;
    }

    /*****************************************************************************/


    //Get Values

   
    public synchronized ErrorsAndHashMap UpdateValuesAndGetLegalityFedback(DocumentStructure DocumentStructure, boolean MarkErrors){
        HashMap<String, Boolean> checkBoxsKeyAndValueList = new HashMap<String, Boolean>();
        HashMap<String, String> editTextsKeyAndValueList = new HashMap<String, String>();
        ArrayList<Error> errorsList = new ArrayList<Error>();

        checkBoxsKeyAndValueList.clear();
        editTextsKeyAndValueList.clear();
        errorsList.clear();
        for (int i = 0; i < DocumentStructure.mObjectsArrayList.size(); i++) {
            try {

                final Object object = DocumentStructure.mObjectsArrayList.get(i);
                String objectName = object.mObejctName;

                if ((object.mObjectKind == Object.OBJECTKIND_CHECKBOX ? checkBoxsKeyAndValueList.get(objectName) : editTextsKeyAndValueList.get(objectName)) != null) {
                    mContext.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //AlertUIDialog alertUIDialog = new AlertUIDialog(mContext, "משהו נכשל ):", "קיימים שני אובייקטים או יותר עם אותו השם");
                            //alertUIDialog.show();
                        }
                    });

                    return null;
                }

                if (object.mObjectKind == Object.OBJECTKIND_CHECKBOX) {
                    final CheckBox checkBox = (CheckBox) object.mObjectView;
                    boolean value = checkBox.isChecked();
                    if (!value) {
                        if (MarkErrors && !object.mHasErrorMarking) {
                            object.mColorBeforeMarking = checkBox.getCurrentTextColor();
                            object.mHasErrorMarking = true;

                            mContext.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    checkBox.setTextColor(mMarkErrorColor);
                                }
                            });

                            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                @Override
                                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                    if (object.mHasErrorMarking && b) {
                                        compoundButton.setTextColor(object.mColorBeforeMarking);
                                        object.mHasErrorMarking = false;
                                        checkBox.setOnCheckedChangeListener(null);
                                    }
                                }
                            });
                        }

                        Error error = new Error();
                        error.mObject = object;
                        error.mMassage = "לא מסומן";
                        errorsList.add(error);
                    }

                    object.mJSONObject.put(Object.JSONOBJECT_OBJECTBOOLEANVALUE_KEY, value);
                    checkBoxsKeyAndValueList.put(objectName, value);

                } else if (object.mObjectKind == Object.OBJECTKIND_EDITTEXT) {
                    final EditText editText = (EditText) object.mObjectView;
                    String value = editText.getText().toString();

                    final String[] errorText = {GetEditTextToleranceExceed(object, value)};

                    if(value.equals("")){
                        mContext.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                editText.setError("חסר ערך");
                            }
                        });

                        errorText[0] = "חסר ערך";

                    } else if(errorText[0] != null){
                        mContext.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                editText.setError(errorText[0]);
                            }
                        });

                    } else {
                        mContext.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                editText.setError(null);
                            }
                        });

                    }

                    if(errorText != null){
                        Error error = new Error();
                        error.mObject = object;
                        error.mMassage = errorText[0];
                        errorsList.add(error);
                    }

                    object.mJSONObject.put(Object.JSONOBJECT_OBJECTSTRINGVALUE_KEY, value);
                    editTextsKeyAndValueList.put(objectName, value);

                }
            } catch (Exception e) {
                mContext.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //AlertUIDialog alertUIDialog = new AlertUIDialog(mContext, "משהו נכשל ):", "נכל בהזנת הערכים");
                        //alertUIDialog.show();
                    }
                });
                return null;
            }
        }

        ErrorsAndHashMap errorsAndHashMap = new ErrorsAndHashMap();
        errorsAndHashMap.mErrorsArrayList = errorsList;
        errorsAndHashMap.mCheckBoxsValuesAndKeyHashMap = checkBoxsKeyAndValueList;
        errorsAndHashMap.mEditTextsValuesAndKeyHashMap = editTextsKeyAndValueList;

        return errorsAndHashMap;
    }

    public synchronized ArrayList<Error> UpdateAllDocumentValuesAndGetLegalityFedback(DocumentStructure DocumentStructure, Document Document, boolean MarkErrors){
        ErrorsAndHashMap errorsAndHashMap = UpdateValuesAndGetLegalityFedback(DocumentStructure, MarkErrors);
        ArrayList<Error> errorArrayList = errorsAndHashMap.mErrorsArrayList;
        HashMap<String, Boolean> CheckBoxsValueAndKeyHashMap = errorsAndHashMap.mCheckBoxsValuesAndKeyHashMap;
        HashMap<String, String> EditTextsValueAndKeyHashMap = errorsAndHashMap.mEditTextsValuesAndKeyHashMap;

        String topList = "", lowList = "";

        Document.mHMCheckBoxs = "";
        ArrayList<String> checkBoxsKeys = new ArrayList<String>(CheckBoxsValueAndKeyHashMap.keySet());
        for (String key : checkBoxsKeys) {
            String value = Boolean.toString(CheckBoxsValueAndKeyHashMap.get(key));
            Document.mHMCheckBoxs += key + "\n" + value + "\n";
            topList += key + mStatisticTab;
            lowList += value + mStatisticTab;
        }
        Document.mQALCheckBoxs = topList + "\n" + lowList;

        Document.mHMEditTexts = "";
        topList = ""; lowList = "";
        ArrayList<String> editTextsKeys = new ArrayList<String>(EditTextsValueAndKeyHashMap.keySet());
        for (String key : editTextsKeys) {
            String value = EditTextsValueAndKeyHashMap.get(key);
            Document.mHMEditTexts += key + "\n" + value + "\n";
            topList += key + mStatisticTab;
            lowList += value + mStatisticTab;
        }
        Document.mQALEditTexts = topList + "\n" + lowList;

        return errorArrayList;
    }

    public class ErrorsAndHashMap {
        public ArrayList<Error> mErrorsArrayList;
        public HashMap<String, Boolean> mCheckBoxsValuesAndKeyHashMap;
        public HashMap<String, String> mEditTextsValuesAndKeyHashMap;

    }

    /*****************************************************************************/

}
