package roiapps.vxqcdocumenteditor;

import android.support.annotation.Nullable;

public class FailureSource extends DynamicObject {

    @Nullable
    public String mMain;
    @Nullable
    public String mSub;
    @Nullable
    public String mExpl;

}
